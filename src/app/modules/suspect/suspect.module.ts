import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";

import { SuspectRoutingModule } from "./suspect-routing.module";
import { SuspectComponent } from "./components/suspect.component";
import { SharedModule } from "@app/shared/shared.module";
import { SuspectListComponent } from "./components/suspect-list/suspect-list.component";
import { AddSuspectComponent } from "./components/add-suspect/add-suspect.component";

@NgModule({
  declarations: [SuspectComponent, SuspectListComponent, AddSuspectComponent],
  imports: [CommonModule, SuspectRoutingModule, SharedModule],
  providers: [DatePipe]
})
export class SuspectModule {}
