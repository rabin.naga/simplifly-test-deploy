import { Injectable } from "@angular/core";
import { HttpClientService } from "@app/core/services/http-client/http-client.service";
import { Observable, Subscriber } from "rxjs";
import { environment } from "@env/environment";
import { Suspect } from "../models/suspect.model";
import { CustomResponse } from "@app/shared/models/custom-response.model";
import { HttpHeaders, HttpParams } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class SuspectService {
  baseIp = environment.baseIP;
  apiPrefix = environment.apiPrefix;

  constructor(private httpClientService: HttpClientService) {}

  getSuspectList(): Observable<CustomResponse> {
    const params = new HttpParams().set(
      "access_token",
      "4e34af1bf66ea2659df614e9399b210c55fdc2c9"
    );
    return this.httpClientService.get(
      `${this.baseIp}${this.apiPrefix}suspect`,
      null,
      params
    );
  }

  getSuspectById(id: number): Observable<CustomResponse> {
    return this.httpClientService.get(
      `${this.baseIp}${this.apiPrefix}suspect/${id}`
    );
  }

  addSuspect(body: Suspect): Observable<CustomResponse> {
    return this.httpClientService.post(
      `${this.baseIp}${this.apiPrefix}suspect`,
      body
    );
  }

  updateSuspect(id: number, body: Suspect): Observable<CustomResponse> {
    return this.httpClientService.put(
      `${this.baseIp}${this.apiPrefix}suspect/edit/${id}`,
      body
    );
  }

  selectedSuspect;
  setSelectedSuspect(suspect: Suspect) {
    this.selectedSuspect = suspect;
  }

  getSelectedSuspect(): Suspect {
    return this.selectedSuspect;
  }

  getBranchList(): Observable<CustomResponse> {
    return this.httpClientService.get(`${this.baseIp}${this.apiPrefix}branch`);
  }

  getLocationList(): Observable<CustomResponse> {
    return this.httpClientService.get(
      `${this.baseIp}${this.apiPrefix}location`
    );
  }
}
