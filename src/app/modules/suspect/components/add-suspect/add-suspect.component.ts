import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LanguageSwitcherService } from "@app/shared/components/language-switcher/services/language-switcher.service";
import { ValidationMessageService } from "@app/shared/services/validation-message/validation-message.service";
import { RegexConst } from "@app/shared/constants/regex.constant";
import { Location, DatePipe } from "@angular/common";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { SuspectService } from "../../services/suspect.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CustomResponse } from "@app/shared/models/custom-response.model";
import { Suspect } from "../../models/suspect.model";

@Component({
  selector: "simpliflysaas-add-suspect",
  templateUrl: "./add-suspect.component.html",
  styleUrls: ["./add-suspect.component.scss"]
})
export class AddSuspectComponent implements OnInit {
  suspectForm: FormGroup;
  submitted: boolean;
  regexConst = RegexConst;

  editMode: boolean;
  selectedSuspect: Suspect;

  branchList: any;
  locationList: any;

  // object of validation msgs that are dynamically added to form controls
  validationMessages = {
    // U.S english validation messages
    enUS: {
      company_name: {
        required: "Company Name is required"
      },
      communication_title_id: {
        required: "Title is required"
      },
      primary_email: {
        pattern: "Email is invalid"
      },
      office_phone: {
        required: "Office Phone is required",
        pattern: "Office Phone is invalid"
      },
      username: {
        required: "Username is required"
      },
      address1: {
        required: "Address1 is required"
      },
      suite_no: {
        pattern: "Suite no. should be number"
      },
      city: {
        required: "City is required"
      },
      state_id: {
        required: "State is required"
      },
      zip: {
        required: "Zip is required",
        pattern: "Zip should be number and 5 character long"
      },
      office_phone2: {
        pattern: "Office Phone2 is invalid"
      },
      state: {
        required: "State is required"
      },
      website: {
        pattern: "Website is invalid"
      },
      linktin: {
        pattern: "Website is invalid"
      },
      facebook: {
        pattern: "Website is invalid"
      },
      twitter: {
        pattern: "Website is invalid"
      },
      location_id: {
        required: "Location is required"
      },
      branch_id: {
        required: "Branch is required"
      }
    },
    // spanish validation messages
    es: {
      company_name: {
        required: "Company Name is required"
      },
      communication_title_id: {
        required: "Title is required"
      },
      primary_email: {
        pattern: "Email is invalid"
      },
      office_phone: {
        required: "Office Phone is required",
        pattern: "Office Phone is invalid"
      },
      username: {
        required: "Username is required"
      },
      address1: {
        required: "Address1 is required"
      },
      suite_no: {
        pattern: "Suite no. should be number"
      },
      city: {
        required: "City is required"
      },
      state_id: {
        required: "State is required"
      },
      zip: {
        required: "Zip is required",
        pattern: "Zip should be number and 5 character long"
      },
      office_phone2: {
        pattern: "Office Phone2 is invalid"
      },
      state: {
        required: "Estado no puede estar vacío"
      },
      website: {
        pattern: "Website is invalid"
      },
      linktin: {
        pattern: "Website is invalid"
      },
      facebook: {
        pattern: "Website is invalid"
      },
      twitter: {
        pattern: "Website is invalid"
      },
      location_id: {
        required: "Location is required"
      },
      branch_id: {
        required: "Branch is required"
      }
    }
  };

  // object for saving error message when validation fails
  formErrors = {
    company_name: "",
    communication_title_id: "",
    primary_email: "",
    office_phone: "",
    username: "",
    address1: "",
    suite_no: "",
    city: "",
    state_id: "",
    zip: "",
    office_phone2: "",
    website: "",
    linktin: "",
    facebook: "",
    twitter: "",
    google_circle: "",
    location_id: "",
    branch_id: ""
  };

  constructor(
    private fb: FormBuilder,
    private languageSwitcherService: LanguageSwitcherService,
    private validationMessageService: ValidationMessageService,
    private location: Location,
    private suspectService: SuspectService,
    private toastrMessageService: ToastrMessageService,
    private router: Router,
    private activatedRotue: ActivatedRoute,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.getBranchDropdownList();
    this.getLocationDropdownList();
    this.buildSuspectForm();
    this.analyzeRoute();

    this.suspectForm.valueChanges.subscribe(() => {
      this.logValidationErrors();
    });
  }

  getBranchDropdownList() {
    this.suspectService.getBranchList().subscribe(response => {
      if (response.status) {
        this.branchList = response.data;
      }
    });
  }

  getLocationDropdownList() {
    this.suspectService.getLocationList().subscribe(response => {
      if (response.status) {
        this.locationList = response.data;
      }
    });
  }

  /**
   * render add suspect or edit suspect according to the route
   */
  analyzeRoute(): void {
    if (this.router.url.includes("edit")) {
      this.editMode = true;
      this.selectedSuspect = this.suspectService.getSelectedSuspect();
      if (!this.selectedSuspect) {
        this.getSelectedSuspect();
      } else {
        this.buildSuspectForm();
      }
    }
  }

  /**
   * calls an API to get the selected suspect for rendering its information on the form.
   */
  getSelectedSuspect(): void {
    const id = this.activatedRotue.snapshot.params.id;
    this.suspectService.getSuspectById(id).subscribe(
      (response: CustomResponse) => {
        if (response.status) {
          this.selectedSuspect = response.data;
          this.buildSuspectForm();
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  buildSuspectForm(): void {
    if (this.selectedSuspect && this.selectedSuspect.start_date) {
      this.selectedSuspect.start_date = this.datePipe.transform(
        new Date(this.selectedSuspect.start_date),
        "M/d/yy"
      );
    }
    this.suspectForm = this.fb.group({
      company_name: [
        this.editMode ? this.selectedSuspect.company_name : "",
        Validators.required
      ],
      communication_title_id: [
        this.editMode ? this.selectedSuspect.communication_title_id : ""
      ],
      start_date: this.editMode ? this.selectedSuspect.start_date : "",
      main_name: this.editMode ? this.selectedSuspect.main_name : "",
      primary_email: [
        this.editMode ? this.selectedSuspect.primary_email : "",
        Validators.pattern(this.regexConst.EMAIL)
      ],
      office_phone: [
        this.editMode ? this.selectedSuspect.office_phone : "",
        [Validators.required, Validators.pattern(this.regexConst.PHONE_NO)]
      ],
      username: [
        this.editMode ? this.selectedSuspect.username : "",
        [Validators.required]
      ],
      password: [],
      address1: [
        this.editMode ? this.selectedSuspect.address1 : "",
        Validators.required
      ],
      address2: [this.editMode ? this.selectedSuspect.address2 : ""],
      suite_no: [
        this.editMode ? this.selectedSuspect.suite_no : "",
        Validators.pattern(this.regexConst.NUMBER)
      ],
      city: [
        this.editMode ? this.selectedSuspect.city : "",
        Validators.required
      ],
      state_id: [
        this.editMode ? this.selectedSuspect.state_id : "",
        Validators.required
      ],
      zip: [
        this.editMode ? this.selectedSuspect.zip : "",
        [Validators.required, Validators.pattern(this.regexConst.ZIP)]
      ],
      office_phone2: [
        this.editMode ? this.selectedSuspect.office_phone2 : "",
        Validators.pattern(this.regexConst.PHONE_NO)
      ],
      fax: this.editMode ? this.selectedSuspect.fax : "",
      branch_id: [
        this.editMode ? this.selectedSuspect.branch_id : "",
        [Validators.required]
      ],
      location_id: [
        this.editMode ? this.selectedSuspect.location_id : "",
        [Validators.required]
      ],
      website: [
        this.editMode ? this.selectedSuspect.website : "",
        [Validators.pattern(this.regexConst.WEBSITE)]
      ],
      linktin: [
        this.editMode ? this.selectedSuspect.linktin : "",
        [Validators.pattern(this.regexConst.WEBSITE)]
      ],
      facebook: [
        this.editMode ? this.selectedSuspect.facebook : "",
        [Validators.pattern(this.regexConst.WEBSITE)]
      ],
      twitter: [
        this.editMode ? this.selectedSuspect.twitter : "",
        [Validators.pattern(this.regexConst.WEBSITE)]
      ],
      google_circle: this.editMode ? this.selectedSuspect.google_circle : "",
      map: this.editMode ? this.selectedSuspect.map : "",
      other2: this.editMode ? this.selectedSuspect.other2 : "",
      other3: this.editMode ? this.selectedSuspect.other3 : "",
      photo: this.editMode ? this.selectedSuspect.photo : "",
      staffing_company_id: 1
    });
  }

  onFileSelect(event): void {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (event: any) => {
        this.suspectForm.patchValue({ photo: event.target.result });
        console.log(event);
      };
    }
    this.suspectForm.markAsDirty();
  }

  /**
   * adds validation message to the form control on violation
   */
  logValidationErrors(): void {
    if (!this.submitted) return;
    this.formErrors = this.validationMessageService.getFormErrors(
      this.suspectForm,
      this.formErrors,
      this.validationMessages
    );
  }

  /**
   * Executes when user clicks on Add Button
   */
  onSubmit(): void {
    this.hasFieldError();

    if (this.suspectForm.valid) this.addSuspect();
  }

  /**
   * verify whether fields have error, if there is scroll to the error field.
   */
  hasFieldError(): void {
    this.submitted = true;

    this.logValidationErrors();
    setTimeout(() => {
      this.validationMessageService.scrollToErrorField(
        document.querySelectorAll(".error-msg")
      );
    }, 100);
  }

  /**
   * Calls API to add the suspect
   */
  addSuspect(): void {
    this.suspectService.addSuspect(this.suspectForm.value).subscribe(
      response => {
        console.log(response);
        if (response.status) {
          this.toastrMessageService.showSuccess(response.data);
          this.router.navigate(["/suspect"]);
        } else {
          this.toastrMessageService.showError(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  /**
   * Executes when user clicks on Edit button
   */
  onEdit(): void {
    this.hasFieldError();

    if (this.suspectForm.invalid) return;

    if (this.suspectForm.pristine) {
      this.onCancel();
      return;
    }

    this.updateSuspect();
  }

  /**
   * Calls update suspect API
   */
  updateSuspect(): void {
    this.suspectService
      .updateSuspect(this.selectedSuspect.suspect_id, this.suspectForm.value)
      .subscribe(
        response => {
          if (response.status) {
            this.toastrMessageService.showSuccess(response.data);
            this.router.navigate(["/suspect"]);
          } else {
            this.toastrMessageService.showError(response.data);
          }
        },
        error => {
          this.toastrMessageService.showError(error);
        }
      );
  }

  /**
   * Executes when user clicks on cancel button
   */
  onCancel(): void {
    this.location.back();
  }
}
