import { Component, OnInit } from "@angular/core";
import { SuspectService } from "../../services/suspect.service";
import { Suspect } from "../../models/suspect.model";
import { CustomResponse } from "@app/shared/models/custom-response.model";
import { Router } from "@angular/router";

@Component({
  selector: "simpliflysaas-suspect-list",
  templateUrl: "./suspect-list.component.html",
  styleUrls: ["./suspect-list.component.scss"]
})
export class SuspectListComponent implements OnInit {
  paginationItem = [
    "All",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "0-9"
  ];

  suspectList: Suspect[];
  suspectListLoading: boolean;
  currentPage = "A";

  constructor(private suspectService: SuspectService, private router: Router) {}

  ngOnInit() {
    this.getSuspectList();
  }

  getSuspectList(): void {
    this.suspectListLoading = true;
    this.suspectService.getSuspectList().subscribe(
      (response: CustomResponse) => {
        console.log(response);
        if (response.status) {
          this.suspectList = response.data;
        }
      },
      error => {
        console.log(error);
        this.suspectListLoading = false;
      },
      () => {
        this.suspectListLoading = false;
      }
    );
  }

  onPageSelect(value): void {
    this.currentPage = value;
  }

  navigateToEditSuspect(suspect: Suspect): void {
    this.suspectService.setSelectedSuspect(suspect);
    this.router.navigate(["/suspect/edit", suspect.suspect_id]);
  }
}
