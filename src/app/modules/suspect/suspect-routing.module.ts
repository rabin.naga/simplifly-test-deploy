import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SuspectComponent } from "./components/suspect.component";
import { AddSuspectComponent } from "./components/add-suspect/add-suspect.component";

const routes: Routes = [
  { path: "", component: SuspectComponent },
  {
    path: "add",
    component: AddSuspectComponent,
    data: {
      breadcrumb: "Add Suspect"
    }
  },
  {
    path: "edit/:id",
    component: AddSuspectComponent,
    data: {
      breadcrumb: "Edit Suspect"
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuspectRoutingModule {}
