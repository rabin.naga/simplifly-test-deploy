import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StaffingCompanyComponent } from './staffing-company/staffing-company.component';

const routes: Routes = [
  {path: '', component: StaffingCompanyComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffingCompanyRoutingModule { }
