import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffingCompanyComponent } from './staffing-company.component';

describe('StaffingCompanyComponent', () => {
  let component: StaffingCompanyComponent;
  let fixture: ComponentFixture<StaffingCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffingCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffingCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
