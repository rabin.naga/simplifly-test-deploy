import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { StaffingCompanyRoutingModule } from "./staffing-company-routing.module";
import { StaffingCompanyComponent } from "./staffing-company/staffing-company.component";
import { SharedModule } from "@app/shared/shared.module";

@NgModule({
  declarations: [StaffingCompanyComponent],
  imports: [CommonModule, StaffingCompanyRoutingModule, SharedModule]
})
export class StaffingCompanyModule {}
