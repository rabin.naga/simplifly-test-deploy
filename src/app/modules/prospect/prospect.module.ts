import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ProspectRoutingModule } from "./prospect-routing.module";
import { ProspectComponent } from "./components/prospect.component";
import { SharedModule } from "@app/shared/shared.module";
import { CompanyInfoComponent } from "./components/prospect-detail/company-info/company-info.component";
import { RunningNotesComponent } from "./components/prospect-detail/running-notes/running-notes.component";
import { AttachmentComponent } from "./components/prospect-detail/attachment/attachment.component";
import { SimpliflysaasDataTableModule } from "../../../../projects/simpliflysaas-data-table/src/public-api";
import { ProspectListComponent } from "./components/prospect-list/prospect-list.component";
import { CommunicationListComponent } from './components/prospect-detail/communication/communication-list/communication-list.component';
import { AddCommunicationComponent } from './components/prospect-detail/communication/add-communication/add-communication.component';
import { ProspectDetailComponent } from './components/prospect-detail/prospect-detail.component';
import { CommunicationComponent } from './components/prospect-detail/communication/communication.component';
import { SubmissionComponent } from './components/prospect-detail/submission/submission.component';
import { TaskComponent } from './components/prospect-detail/task/task.component';
import { CalculationComponent } from './components/prospect-detail/submission/calculation/calculation.component';
import { ProspectCalculationComponent } from './components/prospect-detail/submission/prospect-calculation/prospect-calculation.component';
import { ClassCodeComponent } from './components/prospect-detail/submission/class-code/class-code.component';
import { SubmissionFormComponent } from './components/prospect-detail/submission/submission-form/submission-form.component';
import { ProposalComponent } from './components/prospect-detail/submission/proposal/proposal.component';
import { TaskListComponent } from './components/prospect-detail/task/task-list/task-list.component';
import { TaskDetailComponent } from './components/prospect-detail/task/task-detail/task-detail.component';

@NgModule({
  declarations: [
    ProspectComponent,
    CompanyInfoComponent,
    RunningNotesComponent,
    AttachmentComponent,
    CommunicationComponent,
    CommunicationListComponent,
    AddCommunicationComponent,
    ProspectDetailComponent,
    ProspectListComponent,
    SubmissionComponent,
    TaskComponent,
    CalculationComponent,
    ProspectCalculationComponent,
    ClassCodeComponent,
    SubmissionFormComponent,
    ProposalComponent,
    TaskListComponent,
    TaskDetailComponent
  ],
  imports: [
    CommonModule,
    ProspectRoutingModule,
    SharedModule,
    SimpliflysaasDataTableModule
  ]
})
export class ProspectModule {}
