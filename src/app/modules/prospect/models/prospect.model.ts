export class Prospect {
  id: number;
  company_name: string;
  city: string;
  zip: string;
  assigned: number;
  next_step: number;
  follow_up_date: string;
  office_number: string;
  contacts: Contact[];
}

export class Contact {
  title?: string;
  name?: string;
  email?: string;
  cell_phone?: string;
  email_report?: string;
  primary_contact?: string;
}
