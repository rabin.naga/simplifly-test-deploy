export class Communication {
  id: number;
  type_of_visit: string;
  date_of_visit: string;
  follow_up_date: string;
}
