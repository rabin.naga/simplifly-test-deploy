export class Attachment {
  id: number;
  name: string;
  size: string;
  uploaded_date: string;
}
