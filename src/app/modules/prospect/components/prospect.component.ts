import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ProspectService } from "../services/prospect.service";

@Component({
  selector: "simpliflysaas-prospect",
  templateUrl: "./prospect.component.html",
  styleUrls: ["./prospect.component.scss"]
})
export class ProspectComponent implements OnInit {
  selectedProspectName: string;

  constructor(
    private router: Router,
    private prospectService: ProspectService
  ) {}

  ngOnInit() {}

  gotoCreateProspectPage(): void {
    this.prospectService.clearSelectedProspectItem();
    this.router.navigate(["/prospect/add"]);
  }
}
