import { Component, OnInit, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "simpliflysaas-task-detail",
  templateUrl: "./task-detail.component.html",
  styleUrls: ["./task-detail.component.scss"]
})
export class TaskDetailComponent implements OnInit {
  @Output() onBack: EventEmitter<any> = new EventEmitter();
  constructor() {}

  ngOnInit() {}

  onCancel(): void {
    this.onBack.emit(true);
  }
}
