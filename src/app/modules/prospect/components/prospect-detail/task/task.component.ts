import { Component, OnInit } from "@angular/core";

@Component({
  selector: "simpliflysaas-task",
  templateUrl: "./task.component.html",
  styleUrls: ["./task.component.scss"]
})
export class TaskComponent implements OnInit {
  showTaskDetail: boolean;

  constructor() {}

  ngOnInit() {}

  hideTaskList(task = null) {
    if (task) {
      this.showTaskDetail = true;
    } else {
      this.showTaskDetail = false;
    }
  }
}
