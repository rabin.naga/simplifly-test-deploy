import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  TemplateRef
} from "@angular/core";
import { ProspectService } from "@app/modules/prospect/services/prospect.service";
import { BsModalRef, BsModalService } from "ngx-bootstrap";

@Component({
  selector: "simpliflysaas-task-list",
  templateUrl: "./task-list.component.html",
  styleUrls: ["./task-list.component.scss"]
})
export class TaskListComponent implements OnInit {
  @Output() onTaskSelect: EventEmitter<any> = new EventEmitter();

  taskList: any;
  taskLoading: boolean;

  // add task modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  constructor(
    private prospectService: ProspectService,
    private bsModalService: BsModalService
  ) {}

  ngOnInit() {
    this.getTaskList();
  }

  getTaskList() {
    this.prospectService.getTasklist().subscribe(response => {
      if (response.status) {
        this.taskList = response.data;
      }
    });
  }

  previewTaskDetail(task) {
    this.onTaskSelect.emit(task);
  }

  closeModal() {
    this.modalRef.hide();
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.bsModalService.show(template, this.config);
  }
}
