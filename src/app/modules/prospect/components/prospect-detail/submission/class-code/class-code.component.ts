import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, FormArray } from "@angular/forms";
import { TextboxQuestion } from "../../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-textbox";

@Component({
  selector: "simpliflysaas-class-code",
  templateUrl: "./class-code.component.html",
  styleUrls: ["./class-code.component.scss"]
})
export class ClassCodeComponent implements OnInit {
  classCodeForm: FormGroup;
  classCodes: FormArray;

  // data table
  classCodesLoading: boolean;
  classCodesList: any;
  dataPerPage: number = 5;
  classCodesFormControls = [
    new TextboxQuestion({
      key: "title",
      label: "State"
    }),
    new TextboxQuestion({
      key: "title",
      label: "Class Code"
    }),
    new TextboxQuestion({
      key: "title",
      label: "Description"
    }),
    new TextboxQuestion({
      key: "title",
      label: "No of Employees"
    }),
    new TextboxQuestion({
      key: "title",
      label: "Bill Rate"
    })
  ];

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.buildClassCodeForm();
    this.getClassCodes();
  }

  buildClassCodeForm(): void {
    this.classCodeForm = this.formBuilder.group({
      classCodes: this.formBuilder.array([this.createClassCodeForm()])
    });
  }

  /**
   * Calls API to get class codes
   */
  getClassCodes() {
    // return []
  }

  createClassCodeForm(): FormGroup {
    return this.formBuilder.group({
      state: [""],
      class_code: [""],
      description: [""],
      no_of_employees: [""],
      bill_rate: [""]
    });
  }

  addClassCodeRow(): void {
    this.classCodes = this.classCodeForm.get("classCodes") as FormArray;
    this.classCodes.push(this.createClassCodeForm());
  }

  deleteClassCodeRow(index: number): void {
    (<FormArray>this.classCodeForm.get("classCodes")).removeAt(index);
  }

  addClassCode(): void {
    console.log(this.classCodeForm.value);
  }
}
