import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProspectCalculationComponent } from './prospect-calculation.component';

describe('ProspectCalculationComponent', () => {
  let component: ProspectCalculationComponent;
  let fixture: ComponentFixture<ProspectCalculationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProspectCalculationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProspectCalculationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
