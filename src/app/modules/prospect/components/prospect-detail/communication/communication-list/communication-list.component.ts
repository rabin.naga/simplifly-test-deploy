import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { ProspectService } from "@app/modules/prospect/services/prospect.service";
import { Communication } from "@app/modules/prospect/models/communication.model";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { SessionStorageService } from "@app/shared/services/session-storage/session-storage.service";

@Component({
  selector: "simpliflysaas-communication-list",
  templateUrl: "./communication-list.component.html",
  styleUrls: ["./communication-list.component.scss"]
})
export class CommunicationListComponent implements OnInit {
  @Output() onEdit: EventEmitter<any> = new EventEmitter();
  communicationList: Communication[];

  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  constructor(
    private prospectService: ProspectService,
    private modalService: BsModalService,
    private sessionStorageService: SessionStorageService
  ) {}

  ngOnInit() {
    this.getCommunicationList();
  }

  getCommunicationList(): void {
    this.prospectService.getCommunicationList().subscribe(response => {
      if (response.status) {
        this.communicationList = response.data;
      }
    });
  }

  editCommunication(communication: Communication): void {
    this.onEdit.emit(communication);

    this.prospectService.setSelectedCommunicationItem(communication);

    this.sessionStorageService.setSessionStorageItem(
      "communicationId",
      communication.id.toString()
    );
  }

  openConfirmDialog(communication: Communication): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.data = communication.type_of_visit;
    this.modalRef.content.action = "delete";
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deleteCommunicationById(communication.id);
      }
    });
  }

  deleteCommunicationById(id): void {
    console.log(id);
  }
}
