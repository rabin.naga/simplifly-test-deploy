import { Component, OnInit } from "@angular/core";
import { SessionStorageService } from "@app/shared/services/session-storage/session-storage.service";

@Component({
  selector: "simpliflysaas-communication",
  templateUrl: "./communication.component.html",
  styleUrls: ["./communication.component.scss"]
})
export class CommunicationComponent implements OnInit {
  showAddCommunication: boolean;
  constructor(private sessionStorageService: SessionStorageService) {}

  ngOnInit() {}
  previewAddCommunication() {
    this.showAddCommunication = true;
    this.sessionStorageService.clearSessionStorageItemByKey("communicationId");
  }

  goBack() {
    this.showAddCommunication = false;
  }

  showEditCommunication(event) {
    this.showAddCommunication = true;
  }

  showCommunicationList(event) {
    if (event) {
      this.showAddCommunication = false;
    }
  }
}
