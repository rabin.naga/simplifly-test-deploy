import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Communication } from "@app/modules/prospect/models/communication.model";
import { ProspectService } from "@app/modules/prospect/services/prospect.service";
import { SessionStorageService } from "@app/shared/services/session-storage/session-storage.service";

@Component({
  selector: "simpliflysaas-add-communication",
  templateUrl: "./add-communication.component.html",
  styleUrls: ["./add-communication.component.scss"]
})
export class AddCommunicationComponent implements OnInit {
  @Output() onBack: EventEmitter<any> = new EventEmitter();
  communicationForm: FormGroup;
  editMode: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private prospectService: ProspectService,
    private sessionStorageService: SessionStorageService
  ) {}

  ngOnInit() {
    this.analyzeMode();
    this.buildCommunicationForm();
  }

  selectedCommunication: Communication;
  analyzeMode(): void {
    const communicationId = this.sessionStorageService.getSessionStorageItem(
      "communicationId"
    );
    if (communicationId) {
      this.editMode = true;

      this.selectedCommunication = this.prospectService.getSelectedCommunicationItem();
    }
  }

  buildCommunicationForm(): void {
    this.communicationForm = this.formBuilder.group({
      type_of_visit: [
        this.selectedCommunication
          ? this.selectedCommunication.type_of_visit
          : ""
      ],
      date_of_visit: [
        this.selectedCommunication
          ? this.selectedCommunication.date_of_visit
          : ""
      ],
      follow_up_date: [
        this.selectedCommunication
          ? this.selectedCommunication.follow_up_date
          : ""
      ],
      communication_type: [""],
      notes: [""],
      name: [""],
      email: [""],
      email_template: [""],
      subject: [""],
      description: [""],
      file: [""]
    });
  }

  onFileSelect(event) {}

  onSubmit() {}

  onEdit() {}

  onCancel() {
    this.onBack.emit(true);
  }
}
