import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunningNotesComponent } from './running-notes.component';

describe('RunningNotesComponent', () => {
  let component: RunningNotesComponent;
  let fixture: ComponentFixture<RunningNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunningNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunningNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
