import { Component, OnInit } from "@angular/core";
import { ProspectService } from "../../../services/prospect.service";

@Component({
  selector: "simpliflysaas-running-notes",
  templateUrl: "./running-notes.component.html",
  styleUrls: ["./running-notes.component.scss"]
})
export class RunningNotesComponent implements OnInit {
  runningNotesList: any;

  constructor(private prospectService: ProspectService) {}

  ngOnInit() {
    this.getRunningNotes();
  }

  getRunningNotes(): void {
    this.prospectService.getRunningNotes().subscribe(response => {
      if (response.status) {
        this.runningNotesList = response.data;
      }
    });
  }
}
