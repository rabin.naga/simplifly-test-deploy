import { Component, OnInit } from "@angular/core";
import { TextboxQuestion } from "../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-textbox";
import { ProspectService } from "../../../services/prospect.service";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { Attachment } from "@app/modules/prospect/models/attachment.model";

@Component({
  selector: "simpliflysaas-attachment",
  templateUrl: "./attachment.component.html",
  styleUrls: ["./attachment.component.scss"]
})
export class AttachmentComponent implements OnInit {
  // data table
  attachmentLoading: boolean;
  attachmentList: any;

  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  constructor(
    private prospectService: ProspectService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.getAttachments();
  }

  getAttachments() {
    this.prospectService.getAttachments().subscribe(response => {
      console.log(response);
      if (response.status) {
        this.attachmentList = response.data;
      }
    });
  }

  openConfirmDialog(attachment: Attachment): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.data = attachment.name;
    this.modalRef.content.action = "delete";
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deleteAttachmentById(attachment.id);
      }
    });
  }

  deleteAttachmentById(id) {
    console.log(id);
  }
}
