import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router, NavigationEnd } from "@angular/router";
import { BreadcrumbsService } from "ng6-breadcrumbs";
import { ProspectService } from "../../services/prospect.service";
import { Prospect } from "../../models/prospect.model";
import { HelperService } from "@app/shared/services/helper/helper.service";

@Component({
  selector: "simpliflysaas-prospect-detail",
  templateUrl: "./prospect-detail.component.html",
  styleUrls: ["./prospect-detail.component.scss"]
})
export class ProspectDetailComponent implements OnInit {
  addCompanyForm: boolean;
  selectedProspect: Prospect;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private breadcrumbsService: BreadcrumbsService,
    private prospectService: ProspectService,
    private helperService: HelperService
  ) {
    router.events.subscribe(val => {
      // see also
      if (val instanceof NavigationEnd) {
        if (val.url.includes("add")) {
          this.addCompanyForm = true;
        }
      }
    });
  }

  ngOnInit() {
    this.getSelectedPropsect();

    this.setBreadcrumbItems();
  }

  setBreadcrumbItems() {
    this.breadcrumbsService.store([
      { label: "Dashboard", url: "/", params: [] },
      { label: "Prospect", url: "/prospect", params: [] },
      {
        label: this.helperService.replaceHyphenWithSpace(
          this.selectedProspect.company_name
        ),
        url: "/utitlites/" + this.selectedProspect.id,
        params: []
      }
    ]);
  }

  getSelectedPropsect() {
    this.selectedProspect = this.prospectService.getSelectedProspectItem();
    if (!this.selectedProspect) {
      const prospectId = this.activatedRoute.snapshot.params.id;
      this.getSelectedProspectById(prospectId);
    }
  }

  getSelectedProspectById(id: number) {
    this.prospectService.getProspectById(id).subscribe(response => {
      if (response.status) {
        this.selectedProspect = response.data;
        this.prospectService.setSelectedProspectItem(this.selectedProspect);
      }
    });
  }
}
