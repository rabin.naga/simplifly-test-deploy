import {
  Component,
  OnInit,
  Input,
  OnChanges,
  TemplateRef
} from "@angular/core";
import { FormGroup, FormBuilder, FormArray } from "@angular/forms";
import { ProspectService } from "@app/modules/prospect/services/prospect.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Prospect, Contact } from "@app/modules/prospect/models/prospect.model";
import { BsModalRef, BsModalService } from "ngx-bootstrap";

@Component({
  selector: "simpliflysaas-company-info",
  templateUrl: "./company-info.component.html",
  styleUrls: ["./company-info.component.scss"]
})
export class CompanyInfoComponent implements OnInit, OnChanges {
  @Input() clearForm;
  prospectForm: FormGroup;
  editMode: boolean;
  selectedProspect: Prospect;

  contactForm: FormGroup;
  contacts: FormArray;

  // object for saving error message when validation fails
  formErrors = {
    company_name: "",
    username: "",
    date: "",
    office_number: "",
    extension: "",
    fax_number: "",
    industry: "",
    email: "",
    photo: ""
  };

  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  constructor(
    private formbuilder: FormBuilder,
    private prospectService: ProspectService,
    private router: Router,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.analyzeMode();
  }

  ngOnChanges() {
    if (this.clearForm) {
      this.selectedProspect = null;
      this.editMode = false;
      this.buildProspectForm();
      this.prospectForm.removeControl("contacts");
    }
  }

  analyzeMode(): void {
    if (!this.router.url.includes("add")) {
      this.editMode = true;
      this.selectedProspect = this.prospectService.getSelectedProspectItem();
      this.buildProspectForm();
    } else {
      this.buildProspectForm();
      this.prospectForm.removeControl("contacts");
    }
  }

  buildProspectForm(): void {
    this.prospectForm = this.formbuilder.group({
      company_name: [
        this.selectedProspect ? this.selectedProspect.company_name : ""
      ],
      username: [""],
      date: [""],
      office_number: [
        this.selectedProspect ? this.selectedProspect.office_number : ""
      ],
      extension: [""],
      fax_number: [""],
      industry: [""],
      email: [""],
      photo: [""],
      address1: [""],
      suite_no: [""],
      city: [this.selectedProspect ? this.selectedProspect.city : ""],
      state: [""],
      zip: [this.selectedProspect ? this.selectedProspect.zip : ""],
      branch: [""],
      location: [""],
      contacts: this.selectedProspect
        ? this.buildContactForm()
        : this.formbuilder.array([this.createContactForm()])
    });
  }

  createContactForm(contact: Contact = {}): FormGroup {
    return this.formbuilder.group({
      title: [contact ? contact.title : ""],
      name: [contact ? contact.name : ""],
      email: [contact ? contact.email : ""],
      cell_phone: [contact ? contact.cell_phone : ""],
      email_report: [contact ? contact.email_report : ""],
      primary_contact: [contact ? contact.primary_contact : ""]
    });
  }

  buildContactForm(): FormArray {
    if (!this.selectedProspect.contacts) {
      return this.formbuilder.array([this.createContactForm()]);
    }
    return this.formbuilder.array(
      this.selectedProspect.contacts.map(contact =>
        this.createContactForm(contact)
      )
    );
  }

  onFileSelect(event) {}

  onCancel(): void {
    this.router.navigate(["prospect"]);
  }

  addContactRow(): void {
    this.contacts = this.prospectForm.get("contacts") as FormArray;
    this.contacts.push(this.createContactForm());
  }

  enableContactRow(index: number): void {
    console.log("enableContactRow");
  }

  deleteContactRow(index: number): void {
    (<FormArray>this.prospectForm.get("contacts")).removeAt(index);
  }

  onAdd() {
    console.log(this.prospectForm.value);
  }

  onEdit() {
    console.log(this.prospectForm.value);
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, this.config);
    // this.buildEmailCampaignForm();
  }

  closeModal(): void {
    // setting it to true again to trigger ngOnChanges for closeEditMode for the next edit item
    this.modalRef.hide();
  }
}
