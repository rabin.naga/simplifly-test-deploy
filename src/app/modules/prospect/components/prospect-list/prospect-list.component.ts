import { Component, OnInit } from "@angular/core";
import { ProspectService } from "../../services/prospect.service";
import { Prospect } from "../../models/prospect.model";
import { Router } from "@angular/router";

@Component({
  selector: "simpliflysaas-prospect-list",
  templateUrl: "./prospect-list.component.html",
  styleUrls: ["./prospect-list.component.scss"]
})
export class ProspectListComponent implements OnInit {
  prospectList: Prospect[];
  prospectListLoading: boolean;
  paginationItem = [
    "All",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "0-9"
  ];
  currentPage = "A";

  constructor(
    private prospectService: ProspectService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getProspectList();
  }

  getProspectList(): void {
    this.prospectService.getProspectList().subscribe(response => {
      if (response.status) {
        this.prospectList = response.data;
      }
    });
  }

  navigateToProspectDetail(prospect: Prospect): void {
    this.router.navigate(["/prospect", prospect.id]);
    this.prospectService.setSelectedProspectItem(prospect);
  }

  onPageSelect(value): void {
    this.currentPage = value;
  }
}
