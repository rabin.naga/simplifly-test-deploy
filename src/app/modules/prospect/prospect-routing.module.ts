import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ProspectComponent } from "./components/prospect.component";
import { ProspectDetailComponent } from "./components/prospect-detail/prospect-detail.component";
import { ProspectListComponent } from "./components/prospect-list/prospect-list.component";

const routes: Routes = [
  {
    path: "",
    component: ProspectComponent,
    children: [
      { path: "", component: ProspectListComponent },
      { path: ":id", component: ProspectDetailComponent },
      {
        path: "add",
        component: ProspectDetailComponent,
        data: {
          breadcrumb: "Add Prospect"
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProspectRoutingModule {}
