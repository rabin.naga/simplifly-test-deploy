import { Injectable } from "@angular/core";
import { Observable, Subscriber } from "rxjs";
import { HttpClientService } from "@app/core/services/http-client/http-client.service";
import { environment } from "@env/environment";
import { CustomResponse } from "@app/shared/models/custom-response.model";
import { Communication } from "../models/communication.model";
import { Prospect } from "../models/prospect.model";

@Injectable({
  providedIn: "root"
})
export class ProspectService {
  public apiUrl = `${environment.baseIP}${environment.apiPrefix}`;
  constructor(private httpClientService: HttpClientService) {}

  // ------------------------------------- Prospect --------------------------------------
  getProspectList(): Observable<CustomResponse> {
    const prospectList = {
      status: true,
      data: [
        {
          id: 1,
          company_name: "bentray tech",
          city: "Lalitpur",
          zip: "33600",
          assigned: 1,
          next_step: 2,
          follow_up_date: "05/23/2019 02:50 am",
          office_number: "9860051757",
          contacts: [
            {
              title: "CEO/President",
              name: "Riya",
              email: "riya@bentraytech.com",
              cell_phone: "323-212-1212",
              email_report: true,
              primary_contact: true
            },
            {
              title: "CTO",
              name: "Riya",
              email: "riya@bentraytech.com",
              cell_phone: "323-212-1212",
              email_report: false,
              primary_contact: true
            }
          ]
        },
        {
          id: 2,
          company_name: "logica beans",
          city: "Kathamanduaa aaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaaaaaaa",
          zip: "44800",
          assigned: 2,
          next_step: 3,
          follow_up_date: "05/28/2019 02:50 am",
          office_number: "9818804370"
        }
      ]
    };

    return Observable.create((observer: Subscriber<any>) => {
      observer.next(prospectList);
      observer.complete();
    });

    // return this.httpClientService.get(`${this.apiUrl}`);
  }

  getProspectById(id: number) {
    const prospect = {
      status: true,
      data: {
        id: 1,
        company_name: "bentray tech",
        city: "Lalitpur",
        zip: "33600",
        assigned: 1,
        next_step: 2,
        follow_up_date: "05/23/2019 02:50 am",
        office_number: "9860051757",
        contacts: [
          {
            title: "CEO/President",
            name: "Riya",
            email: "riya@bentraytech.com",
            cell_phone: "323-212-1212",
            email_report: true,
            primary_contact: true
          },
          {
            title: "CTO",
            name: "Riya",
            email: "riya@bentraytech.com",
            cell_phone: "323-212-1212",
            email_report: false,
            primary_contact: true
          }
        ]
      }
    };

    return Observable.create((observer: Subscriber<any>) => {
      observer.next(prospect);
      observer.complete();
    });

    // return this.httpClientService.get(`${this.apiUrl}`);
  }

  selectedProspectItem: Prospect;
  setSelectedProspectItem(prospect: Prospect) {
    this.selectedProspectItem = prospect;
  }

  getSelectedProspectItem(): Prospect {
    return this.selectedProspectItem;
  }

  clearSelectedProspectItem() {
    this.selectedProspectItem = null;
  }
  // ------------------------------------- Running Notes --------------------------------------
  getRunningNotes(): Observable<any> {
    const runningNotesList = {
      status: true,
      data: [
        {
          id: 1,
          image: "asdf.jpg",
          username: "Sanchez Joey",
          date: "05/23/2019 02:50 am",
          message: "Nice"
        },
        {
          id: 2,
          image: "asdf.jpg",
          username: "Elad",
          date: "05/23/2019 02:58 am",
          message: "Thank you!"
        },
        {
          id: 3,
          image: "asdf.jpg",
          username: "Maria",
          date: "05/23/2019 03:00 am",
          message: "I love your website"
        }
      ]
    };

    return Observable.create((observer: Subscriber<any>) => {
      observer.next(runningNotesList);
      observer.complete();
    });

    // return this.httpClientService.get(`${this.apiUrl}`);
  }

  // ------------------------------------- Attachments --------------------------------------
  getAttachments(): Observable<CustomResponse> {
    const runningNotesList = {
      status: true,
      data: [
        {
          id: 1,
          name: "first.jpg",
          size: "82 KB",
          uploaded_date: "05/23/2019 02:50 am"
        },
        {
          id: 2,
          name: "second.jpg",
          size: "182 KB",
          uploaded_date: "05/26/2019 03:50 am"
        },
        {
          id: 3,
          name: "third.jpg",
          size: "2 MB",
          uploaded_date: "05/28/2019 04:00 am"
        }
      ]
    };

    return Observable.create((observer: Subscriber<any>) => {
      observer.next(runningNotesList);
      observer.complete();
    });
  }

  // ------------------------------------- Communication --------------------------------------
  getCommunicationList(): Observable<CustomResponse> {
    const communicationList = {
      status: true,
      data: [
        {
          id: 1,
          type_of_visit: "Lunch Visit",
          date_of_visit: "05/23/2019 02:50 am",
          follow_up_date: "05/25/2019 02:50 am"
        },
        {
          id: 2,
          type_of_visit: "Drop off visit",
          date_of_visit: "05/23/2019 02:50 am",
          follow_up_date: "05/26/2019 03:50 am"
        },
        {
          id: 3,
          type_of_visit: "Operations visit",
          date_of_visit: "05/23/2019 02:50 am",
          follow_up_date: "05/28/2019 04:00 am"
        }
      ]
    };

    return Observable.create((observer: Subscriber<any>) => {
      observer.next(communicationList);
      observer.complete();
    });
  }

  selectedCommunicationItem: Communication;
  setSelectedCommunicationItem(communication: Communication) {
    this.selectedCommunicationItem = communication;
  }

  getSelectedCommunicationItem(): Communication {
    return this.selectedCommunicationItem;
  }

  // ------------------------------------- Submission --------------------------------------

  // ------------------------------------- Task --------------------------------------

  getTasklist(): Observable<CustomResponse> {
    const taskList = {
      status: true,
      data: [
        {
          id: 1,
          title: "one",
          reminder_date: "05/23/2019",
          stage: 1
        },
        {
          id: 2,
          title: "two",
          reminder_date: "05/23/2019",
          stage: 2
        },
        {
          id: 3,
          title: "three",
          reminder_date: "05/23/2019",
          stage: 3
        }
      ]
    };

    return Observable.create((observer: Subscriber<any>) => {
      observer.next(taskList);
      observer.complete();
    });
  }
}
