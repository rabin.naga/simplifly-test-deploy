export class Admin {
  user_id: string;
  admin_id: string;
  address1: string;
  address2?: string;
  city: string;
  state_id: string;
  zip: string;
  created_by?: string;
  created_datetime: string;
  email: string;
  first_name: string;
  middle_name?: string;
  last_name: string;
  phone: string;
  photo: string;
  retention_signature_name: string;
  retention_signature_title: string;

  status: string;
  super_admin: string;
  updated_by?: string;
  updated_datetime: string;

  username: string;

  is_deleted: string;
}
