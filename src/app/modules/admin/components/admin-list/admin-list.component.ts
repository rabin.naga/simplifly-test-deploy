import { Component, OnInit } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { AdminService } from "@app/modules/admin/services/admin.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { HelperService } from "@app/shared/services/helper/helper.service";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { Admin } from "../../models/admin.model";
import { CustomResponse } from "@app/shared/models/custom-response.model";

@Component({
  selector: "simpliflysaas-admin-list",
  templateUrl: "./admin-list.component.html",
  styleUrls: ["./admin-list.component.scss"]
})
export class AdminListComponent implements OnInit {
  adminList: Admin[];
  adminCount: number;
  adminToBeDeleted: Admin;
  adminListLoading: boolean;

  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  // pagination
  itemsPerPage: number = 10;
  currentPage: number = 1;

  // filtering
  isCollapsed: boolean = false;
  searchByName: string;
  searchByAddress: string;
  searchByPhone: string;
  searchByEmail: string;
  searchByRetentionName: string;
  searchByRetentionTitle: string;

  constructor(
    private adminService: AdminService,
    private toastr: ToastrService,
    private router: Router,
    private modalService: BsModalService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    this.getAdmins();
  }

  /**
   * Call get admin API
   */
  getAdmins(): void {
    this.adminListLoading = true;
    this.adminService.getAdminList().subscribe(
      (response: CustomResponse) => {
        if (response.status === true) {
          this.adminList = response.data;
          this.adminCount = response.count;
          return;
        }
      },
      error => {
        this.adminListLoading = false;
      },
      () => {
        this.adminListLoading = false;
      }
    );
  }

  /** Calls delete admin API */
  deleteAdminByAdminId(adminId): void {
    this.adminService.deleteAdmin(adminId).subscribe(
      (response: CustomResponse) => {
        if (response.status === true) {
          this.toastr.success("Admin deleted successfully");
        }
      },
      error => {
        this.toastr.error(error);
      },
      () => {
        this.getAdmins();
      }
    );
  }

  /** Calls edit admin information API */
  editAdminInformation(admin: Admin): void {
    this.adminService.setSelectedAdmin(admin);
    this.router.navigate(["/admin/edit", admin.admin_id], {
      queryParams: { type: "user-information" }
    });
  }

  /**
   * Call edit admin credential PAI
   * @param admin
   */
  editAdminCredential(admin: Admin): void {
    this.adminService.setSelectedAdmin(admin);
    this.router.navigate(["/admin/edit", admin.admin_id], {
      queryParams: { type: "user-credential" }
    });
  }

  openConfirmDialog(admin: Admin): void {
    const data = {
      title: this.helperService.getFullName(admin)
    };
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.data = data.title;
    this.modalRef.content.action = "delete";
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deleteAdminByAdminId(admin.admin_id);
      }
    });
  }

  setCurrentPage(pageNumber): void {
    this.currentPage = pageNumber;
  }

  /**
   * Navigate to AdminDetail Page on clicking username column.
   * @param admin
   */
  navigateToAdminDetail(admin) {
    this.router.navigate(["/admin", admin.admin_id]);
  }

  // sorting logic
  key = "title"; // sort default by name
  reverse = false;
  sortList(key): void {
    this.key = key;
    this.reverse = !this.reverse;
    this.setCurrentPage(1);
  }
}
