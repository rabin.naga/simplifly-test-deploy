import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Route } from "@angular/router";
import { AdminService } from "../../services/admin.service";
import { Admin } from "../../models/admin.model";
import { HelperService } from "@app/shared/services/helper/helper.service";

@Component({
  selector: "simpliflysaas-admin-detail",
  templateUrl: "./admin-detail.component.html",
  styleUrls: ["./admin-detail.component.scss"]
})
export class AdminDetailComponent implements OnInit {
  adminDetail: Admin;
  adminFullName: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private adminService: AdminService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    const admin_id = this.activatedRoute.snapshot.params.id;
    this.getAdminDetailById(admin_id);
  }

  /**
   * Call API to get admin details by id
   * @param id
   */
  getAdminDetailById(id: number): void {
    this.adminService.getAdminDetailById(id).subscribe(
      response => {
        if (response && response.status) {
          this.adminDetail = response.data;
          this.adminFullName = this.getFullName(this.adminDetail);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  getFullName(admin): string {
    return this.helperService.getFullName(admin);
  }
}
