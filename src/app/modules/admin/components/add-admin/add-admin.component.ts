import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { AdminService } from "@app/modules/admin/services/admin.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { checkPassword } from "@app/shared/directives/validators/check-password";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { CustomResponse } from "@app/shared/models/custom-response.model";
import { Admin } from "../../models/admin.model";
import { RegexConst } from "@app/shared/constants/regex.constant";
import { environment } from "@env/environment";
import { Location } from "@angular/common";
@Component({
  selector: "app-add-admin",
  templateUrl: "./add-admin.component.html",
  styleUrls: ["./add-admin.component.scss"]
})
export class AddAdminComponent implements OnInit {
  adminForm: FormGroup;
  submitted: boolean;
  selectedAdmin: Admin;
  editAdminUserInformation: boolean;
  editAdminPassword: boolean;
  uploadingAdmin: boolean; // preview spinner while saving admin
  uploading: boolean; // preview uploading while uploading image
  editMode: boolean;

  regexConst = RegexConst;
  apiUrl = environment.baseIP;
  constructor(
    private adminService: AdminService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private toastrMessageService: ToastrMessageService,
    private location: Location
  ) {}

  ngOnInit() {
    this.analyzeRoute();
    this.buildAdminForm();
  }

  /**
   * render add admin or edit admin according to the route
   */
  analyzeRoute(): void {
    if (this.router.url.includes("edit")) {
      this.editMode = true;
      this.selectedAdmin = this.adminService.getSelectedAdmin();

      this.getQueryParam();
      if (!this.selectedAdmin) this.getSelectedAdmin();
    } else {
      // preview all the fields for add admin
      this.editAdminPassword = true;
      this.editAdminUserInformation = true;
    }
  }

  /**
   * identify admin information or admin credential
   */
  getQueryParam(): void {
    this.activatedRoute.queryParams.subscribe(param => {
      if (param.type === "user-information") {
        // preview only edit user information fields
        this.editAdminUserInformation = true;
        return;
      }
      if (param.type === "user-credential") {
        // preview only user credential fields
        this.editAdminPassword = true;
        return;
      }
    });
  }

  /**
   * Initializes the Admin Form
   */
  buildAdminForm(): void {
    const admin = this.selectedAdmin;

    this.adminForm = this._fb.group(
      {
        username: [
          {
            value: admin ? admin.username : "",
            disabled: this.editMode ? true : false
          },
          Validators.required
        ],
        password: ["", Validators.required],
        confirmPassword: [""],
        first_name: [admin ? admin.first_name : "", Validators.required],
        middle_name: [admin ? admin.middle_name : ""],
        last_name: [admin ? admin.last_name : "", Validators.required],
        address1: [admin ? admin.address1 : "", Validators.required],
        address2: [admin ? admin.address2 : ""],
        city: [admin ? admin.city : "", Validators.required],
        state_id: [admin ? admin.state_id : "", Validators.required],
        zip: [admin ? admin.zip : "", Validators.required],
        phone: [
          admin ? admin.phone : "",
          [Validators.pattern(this.regexConst.PHONE_NO)]
        ],
        email: [
          admin ? admin.email : "",
          [Validators.required, Validators.pattern(this.regexConst.EMAIL)]
        ],
        retention_signature_name: [
          admin ? admin.retention_signature_name : "",
          Validators.required
        ],
        retention_signature_title: [
          admin ? admin.retention_signature_title : "",
          Validators.required
        ],
        photo: [admin ? admin.photo : ""]
      },
      { validator: checkPassword }
    );
  }

  // input form controls names for form validation
  get username() {
    return this.adminForm.get("username");
  }
  get password() {
    return this.adminForm.get("password");
  }
  get first_name() {
    return this.adminForm.get("first_name");
  }
  get last_name() {
    return this.adminForm.get("last_name");
  }
  get address1() {
    return this.adminForm.get("address1");
  }
  get city() {
    return this.adminForm.get("city");
  }
  get state_id() {
    return this.adminForm.get("state_id");
  }
  get zip() {
    return this.adminForm.get("zip");
  }
  get phone() {
    return this.adminForm.get("phone");
  }
  get email() {
    return this.adminForm.get("email");
  }
  get retention_signature_name() {
    return this.adminForm.get("retention_signature_name");
  }
  get retention_signature_title() {
    return this.adminForm.get("retention_signature_title");
  }
  get photo() {
    return this.adminForm.get("photo");
  }

  /**
   * Method to add Admin
   */
  addAdmin(): void {
    this.submitted = true;
    if (this.adminForm.invalid) return;

    this.uploadingAdmin = true;
    this.adminService.addAdmin(this.adminForm.value).subscribe(
      (response: any) => {
        if (response.status === true) {
          this.toastrMessageService.showSuccess("Admin added successfully");
          this.router.navigate(["/admin"]);
          return;
        }

        this.toastrMessageService.showError(response.data);
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.uploadingAdmin = false;
      }
    );
  }

  /**
   * get the selected admin data
   */
  getSelectedAdmin(): void {
    const adminId = this.activatedRoute.snapshot.params["id"];

    this.adminService.getAdminByAdminId(adminId).subscribe(
      response => {
        if (response.status === true) {
          this.selectedAdmin = response.data;
          this.buildAdminForm();
          return;
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  /**
   * call update admin API for editing admin information
   */
  editAdminInformation(): void {
    this.submitted = true;

    if (
      this.adminForm.controls["password"] &&
      this.adminForm.controls["password"].errors
    ) {
      this.adminForm.controls["password"].setErrors(null);
    }

    if (this.adminForm.invalid) return;
    if (this.adminForm.valid && this.adminForm.pristine) {
      this.router.navigate(["/admin"]);
      return;
    }

    this.adminForm.removeControl("username");
    this.adminForm.removeControl("password");
    this.adminForm.removeControl("confirmPassword");

    this.uploadingAdmin = true;

    const adminId = this.selectedAdmin.admin_id;
    this.adminService.editAdmin(adminId, this.adminForm.value).subscribe(
      (response: CustomResponse) => {
        if (response.status === true) {
          this.toastrMessageService.showSuccess("Admin edited successfully");
          this.router.navigate(["/admin"]);
          return;
        }
        this.toastrMessageService.showError(response.data);
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.uploadingAdmin = false;
      }
    );
  }

  /**
   * call update admin API to edit admin credentials
   */
  editAdminCredential(): void {
    this.submitted = true;

    if (this.adminForm.invalid) return;

    const body = {
      password: this.password.value
    };

    this.uploadingAdmin = true;
    const userId = this.selectedAdmin.user_id;
    this.adminService.editAdminCredential(userId, body).subscribe(
      (response: CustomResponse) => {
        if (response.status === true) {
          this.toastrMessageService.showSuccess("Admin edited successfully");
          this.router.navigate(["/admin"]);
          return;
        }
        this.toastrMessageService.showError(response.data);
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.uploadingAdmin = false;
      }
    );
  }

  url: any = "";
  /**
   * called each time file input changes
   * @param event - image data
   */
  onSelectFile(event): void {
    // file is not selected i.e user clicks on cancel while uploading
    if (event.target.files.length === 0) {
      // remove validation error messages
      if (this.adminForm.controls["photo"].hasError("invalidImageSize")) {
        delete this.adminForm.controls["photo"].errors["invalidImageSize"];
      }

      if (this.adminForm.controls["photo"].hasError("invalidImageType")) {
        delete this.adminForm.controls["photo"].errors["invalidImageType"];
      }
      // remove image preview
      this.url = "";
      this.adminForm.controls["photo"].updateValueAndValidity();
    }

    // file is selected
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      // verify whether the uploaded file is image or not
      if (!event.target.files[0].type.match("image/", /.(jpg|jpeg|png)$/i)) {
        // this.uploading = false;
        this.url = "";
        this.adminForm.controls["photo"].setErrors({ invalidImageType: true });
        return;
      }

      // verify whether the file is more than 2MB
      if (event.target.files[0].size > 2097152) {
        // 2097152 is equal to 2 MB
        this.url = event.target.result;
        // this.uploading = false;
        this.adminForm.controls["photo"].setErrors({ invalidImageSize: true });
        reader.onload = (event: any) => {
          this.url = event.target.result;
        };
        this.adminForm.updateValueAndValidity();
        return;
      }

      this.uploading = true;
      reader.onload = (event: any) => {
        // called once readAsDataURL is completed
        this.url = event.target.result;
        this.uploading = false;
        this.adminForm.patchValue({ photo: event.target.result });
        this.adminForm.markAsDirty();
      };
    }
  }

  onCancel(): void {
    this.location.back();
  }
}
