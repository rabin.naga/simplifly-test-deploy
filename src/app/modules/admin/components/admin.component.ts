import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";
import { LanguageSwitcherService } from "@app/shared/components/language-switcher/services/language-switcher.service";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.scss"]
})
export class AdminComponent implements OnInit {
  langSubscription: Subscription;

  constructor(
    private translate: TranslateService,
    private languageSwitcherService: LanguageSwitcherService
  ) {
    this.initLanguage();
  }

  ngOnInit() {}

  initLanguage() {
    let lang = this.languageSwitcherService.getSelectedLanguage();

    if (lang) this.translate.use(lang);

    this.langSubscription = this.languageSwitcherService
      .getLanguage()
      .subscribe(response => {
        lang = response.language;
        this.translate.use(lang);
      });
  }
}
