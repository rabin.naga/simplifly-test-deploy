import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AddAdminComponent } from "./components/add-admin/add-admin.component";
import { AdminComponent } from "./components/admin.component";
import { AdminDetailComponent } from "./components/admin-detail/admin-detail.component";

const routes: Routes = [
  { path: "", component: AdminComponent },
  {
    path: "add",
    component: AddAdminComponent,
    data: {
      breadcrumb: "Add Admin"
    }
  },
  {
    path: "edit/:id",
    component: AddAdminComponent,
    data: {
      breadcrumb: "Edit Admin"
    }
  },
  {
    path: ":id",
    component: AdminDetailComponent,
    data: {
      breadcrumb: "Admin Detail"
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
