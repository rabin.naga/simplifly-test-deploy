import { Pipe, PipeTransform } from "@angular/core";
import { HelperService } from "@shared/services/helper/helper.service";

@Pipe({
  name: "adminFilter"
})
export class AdminFilterPipe implements PipeTransform {
  constructor(private helperService: HelperService) {}

  transform(
    items: any[],
    nameSearch: string,
    searchByAddress: any,
    searchByPhone: any,
    searchByEmail: string,
    searchByRetentionName: string,
    searchByRetentionTitle: string
  ) {
    if (items && items.length) {
      return items.filter(item => {
        const fullName = this.helperService.getFullName(item);
        const phoneNumber = item.phone ? item.phone.toString() : "";
        const fullAddress = item.address1
          .toString()
          .concat(
            item.address2 ? item.address2.toString() : "",
            " ",
            item.city,
            " ",
            item.zip
          );

        // filter by full name
        if (
          nameSearch &&
          fullName.toLowerCase().indexOf(nameSearch.toLowerCase().trim()) === -1
        ) {
          return false;
        }

        // filter by full address
        if (
          searchByAddress &&
          fullAddress
            .toLowerCase()
            .indexOf(searchByAddress.toLowerCase().trim()) === -1
        ) {
          return false;
        }

        // filter by phone number
        if (searchByPhone && !phoneNumber.includes(searchByPhone.trim())) {
          return false;
        }

        // filter by email
        if (
          searchByEmail &&
          item.email
            .toLowerCase()
            .indexOf(searchByEmail.toLowerCase().trim()) === -1
        ) {
          return false;
        }

        // filter by retention name
        if (
          searchByRetentionName &&
          item.retention_signature_name
            .toLowerCase()
            .indexOf(searchByRetentionName.toLowerCase().trim()) === -1
        ) {
          return false;
        }

        // filter by retention sign
        if (
          searchByRetentionTitle &&
          item.retention_signature_title
            .toLowerCase()
            .indexOf(searchByRetentionTitle.toLowerCase().trim()) === -1
        ) {
          return false;
        }
        return true;
      });
    } else {
      return items;
    }
  }
}
