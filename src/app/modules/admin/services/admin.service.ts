import { Injectable } from "@angular/core";
import { environment } from "@env/environment";
import { Observable } from "rxjs";
import { ApiRouteConst } from "@app/shared/constants/api-route.constant";
import { HttpClientService } from "@app/core/services/http-client/http-client.service";

@Injectable({
  providedIn: "root"
})
export class AdminService {
  selectedAdmin: any;
  baseIp = environment.baseIP;
  apiPrefix = environment.apiPrefix;
  adminRouteConst = ApiRouteConst.ADMIN;

  constructor(private httpClientService: HttpClientService) {}

  getAdminList(): Observable<any> {
    return this.httpClientService.get(`${this.baseIp}${this.apiPrefix}admin`);
  }

  getAdminByAdminId(adminId): Observable<any> {
    return this.httpClientService.get(
      `${this.baseIp}${this.apiPrefix}admin/${adminId}`
    );
  }

  deleteAdmin(id): Observable<any> {
    return this.httpClientService.delete(
      `${this.baseIp}${this.apiPrefix}admin/${id}`
    );
  }

  addAdmin(body): Observable<any> {
    return this.httpClientService.post(
      `${this.baseIp}${this.apiPrefix}admin`,
      body
    );
  }

  editAdmin(adminId, requestBody): Observable<any> {
    return this.httpClientService.put(
      `${this.baseIp}${this.apiPrefix}admin/edit/${adminId}`,
      requestBody
    );
  }

  editAdminCredential(userId, requestBody): Observable<any> {
    return this.httpClientService.put(
      `${this.baseIp}${this.apiPrefix}admin/edit-login/${userId}`,
      requestBody
    );
  }

  setSelectedAdmin(admin): void {
    this.selectedAdmin = admin;
  }

  getSelectedAdmin() {
    return this.selectedAdmin;
  }

  public getAdminDetailById(id: number): Observable<any> {
    return this.httpClientService.get(
      `${this.baseIp}${this.apiPrefix}admin/${id}`
    );
  }
}
