import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AdminRoutingModule } from "./admin-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared/shared.module";
import { AdminComponent } from "./components/admin.component";
import { AddAdminComponent } from "./components/add-admin/add-admin.component";
import { AdminListComponent } from "./components/admin-list/admin-list.component";
import { AdminDetailComponent } from "./components/admin-detail/admin-detail.component";
import { AdminFilterPipe } from "./services/admin-filter.pipe";
import { HttpClient } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/admin/", ".json");
}
@NgModule({
  declarations: [
    AdminComponent,
    AddAdminComponent,
    AdminListComponent,
    AdminDetailComponent,
    AdminFilterPipe
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      },
      isolate: true
    })
  ]
})
export class AdminModule {}
