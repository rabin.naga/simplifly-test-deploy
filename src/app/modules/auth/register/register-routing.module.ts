import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RegisterComponent } from "./components/register.component";
import { ApplicantRegisterComponent } from "./components/applicant-register/applicant-register.component";

const routes: Routes = [
  { path: "", component: RegisterComponent },
  { path: "applicant", component: ApplicantRegisterComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule {}
