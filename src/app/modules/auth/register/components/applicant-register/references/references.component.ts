import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FormGroup, Validators, FormArray, FormBuilder } from "@angular/forms";
import { ValidationMessageService } from "@app/shared/services/validation-message/validation-message.service";

@Component({
  selector: "simpliflysaas-references",
  templateUrl: "./references.component.html",
  styleUrls: ["./references.component.scss"]
})
export class ReferencesComponent implements OnInit {
  @Output() referenceFormSubmit: EventEmitter<any> = new EventEmitter();
  @Output() onBack: EventEmitter<any> = new EventEmitter();
  referenceForm: FormGroup;
  submitted: boolean;

  constructor(
    private fb: FormBuilder,
    private validationMessageService: ValidationMessageService
  ) {}

  ngOnInit() {
    this.buildReferenceForm();
  }

  buildReferenceForm(): void {
    this.referenceForm = this.fb.group({
      referenceFormArray: this.fb.array([this.referenceFormGroup()])
    });
  }

  /**
   * reference form
   */
  referenceFormGroup(): FormGroup {
    return this.fb.group({
      referenceName: ["", [Validators.required]],
      compnay: ["", Validators.required],
      title: [],
      phone: [],
      email: [],
      relationship: []
    });
  }

  /**
   * Executes on click of Add button
   */
  addReference(): void {
    this.submitted = true;
    this.scrollToErrorField();
    if (this.referenceForm.invalid) {
      return;
    }

    (<FormArray>this.referenceForm.get("referenceFormArray")).push(
      this.referenceFormGroup()
    );
    this.submitted = false;
  }

  get formArray(): FormArray {
    return this.referenceForm.get("referenceFormArray") as FormArray;
  }

  /**
   * Delete Particular Reference
   * @param index
   */
  deleteReference(index: number): void {
    this.formArray.removeAt(index);
  }

  onSubmit(): void {
    this.submitted = true;
    this.scrollToErrorField();
    this.referenceFormSubmit.emit(this.referenceForm);
  }

  goBack(): void {
    this.onBack.emit(true);
  }

  scrollToErrorField() {
    setTimeout(() => {
      this.validationMessageService.scrollToErrorField(
        document.querySelectorAll(".error-msg")
      );
    }, 100);
  }
}
