import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantStatementComponent } from './applicant-statement.component';

describe('ApplicantStatementComponent', () => {
  let component: ApplicantStatementComponent;
  let fixture: ComponentFixture<ApplicantStatementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantStatementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
