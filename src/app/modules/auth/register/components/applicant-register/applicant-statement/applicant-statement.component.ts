import {
  Component,
  OnInit,
  Renderer2,
  Inject,
  Output,
  EventEmitter
} from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { DOCUMENT } from "@angular/common";
@Component({
  selector: "simpliflysaas-applicant-statement",
  templateUrl: "./applicant-statement.component.html",
  styleUrls: ["./applicant-statement.component.scss"]
})
export class ApplicantStatementComponent implements OnInit {
  @Output() applicantStatementFormSubmit: EventEmitter<
    any
  > = new EventEmitter();
  @Output() onBack: EventEmitter<any> = new EventEmitter();
  applicantStatementForm: FormGroup;
  siteKey: any;
  constructor(
    private formBuilder: FormBuilder,
    private renderer2: Renderer2,
    @Inject(DOCUMENT) private _document: Document
  ) {}

  ngOnInit() {
    this.siteKey = "6LdeAK8UAAAAANkasq8IHotd2PLk0nZS-EkEmStz";
    this.buildApplicantStatementForm();

    const s = this.renderer2.createElement("script");
    s.type = "text/javascript";
    s.src = "https://www.google.com/recaptcha/api.js";
    s.text = ``;
    this.renderer2.appendChild(this._document.body, s);
  }

  buildApplicantStatementForm() {
    let current_datetime = new Date();
    let formatted_date =
      current_datetime.getFullYear() +
      "-" +
      (current_datetime.getMonth() + 1) +
      "-" +
      current_datetime.getDate();
    this.applicantStatementForm = this.formBuilder.group({
      name: [],
      date: [{ value: formatted_date, disabled: true }],
      recaptcha: [""]
    });
  }

  submitted: boolean;
  onSubmit() {
    this.submitted = true;
    this.applicantStatementFormSubmit.emit(this.applicantStatementForm);
  }

  goBack() {
    this.onBack.emit(true);
  }
}
