import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { LanguageSwitcherService } from "@app/shared/components/language-switcher/services/language-switcher.service";
import { Subscription } from "rxjs";
import { ValidationMessageService } from "@app/shared/services/validation-message/validation-message.service";

@Component({
  selector: "simpliflysaas-questionnaire",
  templateUrl: "./questionnaire.component.html",
  styleUrls: ["./questionnaire.component.scss"]
})
export class QuestionnaireComponent implements OnInit {
  @Output() questionnaireFormSubmit: EventEmitter<any> = new EventEmitter();
  @Output() onBack: EventEmitter<any> = new EventEmitter();

  questionnaireForm: FormGroup;
  language: string;
  langSubscription: Subscription;

  options = [
    {
      id: 1,
      value: "Y",
      title: "Yes"
    },
    {
      id: 2,
      value: "N",
      title: "No"
    }
  ];

  shiftOptions = [
    {
      id: 1,
      title: "1st Shift"
    },
    {
      id: 2,
      title: "2nd Shift"
    },
    {
      id: 3,
      title: "3rd Shift"
    },
    {
      id: 4,
      title: "1st & 2nd Shift"
    }
  ];

  // learn about Temp-On-Data source
  sourceOptions = [
    {
      id: 1,
      title: "Advertising"
    },
    {
      id: 2,
      title: "Job Fair"
    },
    {
      id: 3,
      title: "Website"
    }
  ];

  desiredPositionOptions = [
    {
      id: 1,
      title: "Customer Service"
    },
    {
      id: 2,
      title: "Leads"
    },
    {
      id: 3,
      title: "Sales"
    }
  ];

  validationMessages = {
    enUS: {
      isEighteenPlus: {
        required: "Required"
      },
      elligibleInUS: {
        required: "Required"
      },
      hasTransportation: {
        required: "Required"
      }
    },
    es: {
      isEighteenPlus: {
        required: "Necesaria"
      },
      elligibleInUS: {
        required: "Necesaria"
      },
      hasTransportation: {
        required: "Necesaria"
      }
    }
  };

  formErrors = {
    isEighteenPlus: "",
    elligibleInUS: "",
    hasTransportation: ""
  };

  constructor(
    private formBuilder: FormBuilder,
    private validationMessageService: ValidationMessageService,
    private languageSwitcherService: LanguageSwitcherService
  ) {}

  ngOnInit() {
    this.buildQuestionnaireForm();
    this.initLanguage();
    this.questionnaireForm.valueChanges.subscribe(data => {
      this.logValidationErrors();
    });
  }

  /**
   * sets the language selected
   */
  initLanguage() {
    this.language = this.languageSwitcherService.getSelectedLanguage();

    if (this.language && this.submitted) this.logValidationErrors();

    this.langSubscription = this.languageSwitcherService
      .getLanguage()
      .subscribe(response => {
        this.language = response.language;
        this.logValidationErrors();
      });
  }

  /**
   * adds validation message to the form control on violation
   */
  logValidationErrors(): void {
    if (!this.submitted) return;
    this.formErrors = this.validationMessageService.getFormErrors(
      this.questionnaireForm,
      this.formErrors,
      this.validationMessages,
      this.language
    );
  }

  /**
   * initialize questionnaire form
   */
  buildQuestionnaireForm(): void {
    this.questionnaireForm = this.formBuilder.group({
      isEighteenPlus: ["", Validators.required],
      elligibleInUS: ["", Validators.required],
      hasTransportation: ["", Validators.required]
    });
  }

  /**
   * executes on next button click
   */
  submitted: boolean;
  onSubmit(): void {
    this.submitted = true;
    this.logValidationErrors();
    setTimeout(() => {
      this.validationMessageService.scrollToErrorField(
        document.querySelectorAll(".error-msg")
      );
    }, 100);
    this.questionnaireFormSubmit.emit(this.questionnaireForm);
  }

  /**
   * executes on back button click
   */
  goBack(): void {
    this.onBack.emit(true);
  }
}
