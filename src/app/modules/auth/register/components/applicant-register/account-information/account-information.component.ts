import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { checkPassword } from "@app/shared/directives/validators/check-password";
import { checkEmail } from "@app/shared/directives/validators/check-email";
import { RegexConst } from "@app/shared/constants/regex.constant";
import { ValidationMessageService } from "@app/shared/services/validation-message/validation-message.service";
import { LanguageSwitcherService } from "@app/shared/components/language-switcher/services/language-switcher.service";
import { Subscription } from "rxjs";

@Component({
  selector: "simpliflysaas-account-information",
  templateUrl: "./account-information.component.html",
  styleUrls: ["./account-information.component.scss"]
})
export class AccountInformationComponent implements OnInit {
  @Output() accountInformationFormSubmit: EventEmitter<
    any
  > = new EventEmitter();
  @Output() onBack: EventEmitter<any> = new EventEmitter();
  accountInformationForm: FormGroup;
  regexConst = RegexConst;

  emailTypeOptions = [{ id: 1, title: "Personal" }];

  // error messages which is added to the form controls
  validationMessages = {
    enUS: {
      email: {
        required: "Email cannot be blank",
        pattern: "Email is not a valid email address"
      },
      emailType: {
        required: "Email Type cannot be blank"
      },
      password: {
        required: "Password cannot be blank"
      },
      emailMismatch: "Email don't match",
      passwordMismatch: "Passwords do not match"
    },
    es: {
      email: {
        required: "Correo Electrónico no puede estar vacío",
        pattern: "Correo Electrónico no es una dirección de correo válida"
      },
      emailType: {
        required:
          "Escriba nuevamente el correo electrónico no puede estar vacío"
      },
      password: {
        required: "Contraseña no puede estar vacío"
      },
      emailMismatch: "Email don't match",
      passwordMismatch: "Passwords do not match"
    }
  };

  // object for saving error message when validation fails
  formErrors = {
    email: "",
    emailType: "",
    password: ""
  };

  constructor(
    private formBuilder: FormBuilder,
    private validationMessageService: ValidationMessageService,
    private languageSwitcherService: LanguageSwitcherService
  ) {}

  ngOnInit() {
    this.buildAccountInformationForm();
    this.initLanguage();
    this.accountInformationForm.valueChanges.subscribe(data => {
      this.logValidationErrors();
    });
  }

  /**
   * sets the selected language
   */
  langSubscription: Subscription;
  language: string;
  initLanguage() {
    this.language = this.languageSwitcherService.getSelectedLanguage();

    if (this.language) this.logValidationErrors();

    this.langSubscription = this.languageSwitcherService
      .getLanguage()
      .subscribe(response => {
        this.language = response.language;
        this.logValidationErrors();
      });
  }

  /**
   * adds validation message to the form control on violation
   */
  logValidationErrors(): void {
    if (!this.submitted) return;
    this.formErrors = this.validationMessageService.getFormErrors(
      this.accountInformationForm,
      this.formErrors,
      this.validationMessages,
      this.language
    );
  }

  buildAccountInformationForm() {
    this.accountInformationForm = this.formBuilder.group(
      {
        email: [
          "",
          [Validators.required, Validators.pattern(this.regexConst.EMAIL)]
        ],
        confirmEmail: [""],
        emailType: ["", Validators.required],
        password: ["", Validators.required],
        confirmPassword: [""],
        monStartTime: [new Date()],
        monEndTime: [new Date()]
      },
      { validators: [checkEmail, checkPassword] }
    );
  }

  submitted: boolean;
  onSubmit() {
    this.submitted = true;
    this.logValidationErrors();
    this.scrollToErrorField();
    this.accountInformationFormSubmit.emit(this.accountInformationForm);
  }

  goBack() {
    this.onBack.emit(true);
  }

  scrollToErrorField() {
    setTimeout(() => {
      this.validationMessageService.scrollToErrorField(
        document.querySelectorAll(".error-msg")
      );
    }, 100);
  }
}
