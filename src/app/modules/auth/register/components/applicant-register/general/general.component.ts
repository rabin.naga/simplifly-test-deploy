import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { LanguageSwitcherService } from "@app/shared/components/language-switcher/services/language-switcher.service";
import { BsDatepickerConfig } from "ngx-bootstrap";
import { ValidationMessageService } from "@app/shared/services/validation-message/validation-message.service";

@Component({
  selector: "simpliflysaas-general",
  templateUrl: "./general.component.html",
  styleUrls: ["./general.component.scss"]
})
export class GeneralComponent implements OnInit {
  @Output() generalFormSubmit: EventEmitter<any> = new EventEmitter();
  @Output() onBack: EventEmitter<any> = new EventEmitter();

  bsConfig: Partial<BsDatepickerConfig>;
  generalForm: FormGroup;

  //masking
  public phoneNumberMask = {
    guide: true,
    showMask: true,
    mask: [/\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/]
  };

  public zipMask = {
    guide: true,
    showMask: true,
    mask: [/\d/, /\d/, /\d/, /\d/]
  };

  public ssnMask = {
    guide: false,
    modelClean: true,
    showMask: true,
    mask: [/\d/, /\d/, /\d/, "-", /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/]
  };

  // drop down options
  branchDropdownOptions = [
    {
      id: 1,
      title: "Los Angles Country"
    },
    {
      id: 2,
      title: "Reverside Country"
    }
  ];

  locationDropdownOptions = [
    {
      id: 1,
      title: "East Los Angeles"
    },
    {
      id: 2,
      title: "Hayward"
    }
  ];

  stateDropdownOptions = [
    {
      id: 1,
      title: "Alabama"
    },
    {
      id: 2,
      title: "Alaska"
    }
  ];

  phoneTypeOptions = [
    {
      id: 1,
      title: "Cell"
    },
    {
      id: 2,
      title: "Phone"
    }
  ];

  maritalStatusDropdownOptions = [
    {
      id: 1,
      title: "Widowed"
    },
    {
      id: 2,
      title: "Single"
    }
  ];

  raceDropdownOptions = [
    {
      id: 1,
      title: "Alaskan Native"
    },
    {
      id: 2,
      title: "Asian"
    }
  ];

  veteranStatusDropdownOptions = [
    {
      id: 1,
      title: "Active"
    },
    {
      id: 2,
      title: "Retired"
    }
  ];

  // object of validation msgs that are dynamically added to form controls
  validationMessages = {
    // U.S english validation messages
    enUS: {
      branch: {
        required: "Branch is required"
      },
      location: {
        required: "Location is required"
      },
      firstName: {
        required: "First name is required",
        minlength: "First name must be greater than 2 characters",
        maxlength: "First name must be less than 10 characters"
      },
      middleName: {},
      lastName: {
        required: "Last name is required"
      },
      ssn: {
        required: "SSN is required",
        minlength: "SSN should be exactly 9 characters"
      },
      state: {
        required: "State is required"
      }
    },
    // spanish validation messages
    es: {
      branch: {
        required: "Branch no puede estar vacío"
      },
      location: {
        required: "Location no puede estar vacío"
      },
      firstName: {
        required: "Nombre no puede estar vacío"
      },
      middleName: {},
      lastName: {
        required: "Apellido no puede estar vacío"
      },
      ssn: {
        required: "Número De Seguridad Social no puede estar vacío",
        minlength: "Ssn should be exactly 9 characters"
      },
      state: {
        required: "Estado no puede estar vacío"
      }
    }
  };

  // object for saving error message when validation fails
  formErrors = {
    branch: "",
    location: "",
    firstName: "",
    middleName: "",
    lastName: "",
    ssn: "",
    state: ""
  };

  constructor(
    private fb: FormBuilder,
    private languageSwitcherService: LanguageSwitcherService,
    private validationMessageService: ValidationMessageService
  ) {}

  langSubscription: Subscription;
  language: string;
  ngOnInit() {
    this.buildgeneralForm();
    this.initLanguage();
    // loads the logvalidationErrors method when form value changes
    this.generalForm.valueChanges.subscribe(data => {
      this.logValidationErrors();
    });
  }

  /**
   * sets the selected language
   */
  initLanguage() {
    this.language = this.languageSwitcherService.getSelectedLanguage();

    if (this.language) this.logValidationErrors();

    this.langSubscription = this.languageSwitcherService
      .getLanguage()
      .subscribe(response => {
        this.language = response.language;
        this.logValidationErrors();
      });
  }

  /**
   * adds validation message to the form control on violation
   */
  logValidationErrors(): void {
    if (!this.submitted) return;
    this.formErrors = this.validationMessageService.getFormErrors(
      this.generalForm,
      this.formErrors,
      this.validationMessages,
      this.language
    );
  }

  /**
   * initialize general form
   */
  buildgeneralForm(): void {
    this.generalForm = this.fb.group({
      branch: ["", Validators.required],
      location: ["", Validators.required],
      firstName: ["", [Validators.required]],
      middleName: [],
      lastName: ["", Validators.required],
      gender: [""],
      dateOfBirth: [""],
      ssn: ["", [Validators.required, Validators.minLength(11)]],
      phoneType1: [""],
      phoneNumber1: [""],
      phoneType2: [""],
      phoneNumber2: [""],
      city: [""],
      zip: [""],
      streetAddress: [""],
      maritalStatus: [""],
      race: [""],
      veteranStatus: [""],
      emergencyContactName: [""],
      emergencyContactRelationship: [""],
      emergencyContactPhoneNumber: [""],
      state: ["", Validators.required]
    });
  }

  submitted: boolean;
  onSubmit(): void {
    this.submitted = true;
    this.logValidationErrors();
    setTimeout(() => {
      this.validationMessageService.scrollToErrorField(
        document.querySelectorAll(".error-msg")
      );
    }, 100);
    this.generalFormSubmit.emit(this.generalForm);
  }

  goBack(): void {
    this.onBack.emit(true);
  }
}
