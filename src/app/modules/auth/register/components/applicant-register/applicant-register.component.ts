import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Location } from "@angular/common";
import { MatStepper } from "@angular/material";
import { Router } from "@angular/router";

@Component({
  selector: "simpliflysaas-applicant-register",
  templateUrl: "./applicant-register.component.html",
  styleUrls: ["./applicant-register.component.scss"]
})
export class ApplicantRegisterComponent implements OnInit {
  @ViewChild("stepper") stepper: MatStepper;

  generalForm: FormGroup;
  questionnaireForm: FormGroup;
  employementHistoryForm: FormGroup;
  referenceForm: FormGroup;
  accountInformationForm: FormGroup;
  applicantStatementForm: FormGroup;

  isAgreed: boolean;

  constructor(private location: Location, private router: Router) {}

  ngOnInit() {}

  /**
   * return to the register page on click of Decline button in E-signature step
   */
  goBack(): void {
    this.location.back();
  }

  /**
   * move to the general step on click of Agree button in E-signature step
   */
  agreeTerms(): void {
    this.isAgreed = true;
    // move to the next step forcefully i.e general step
    this.stepper.selected.completed = true;
    this.stepper.selectedIndex = 1;
  }

  /**
   * stop the user from going to General Step without clicking of Agree button
   */
  restrictToGeneralStep(): void {
    this.isAgreed = false;
  }

  /**
   * executes on change of step
   * @param event
   */
  selectionChange(event): void {
    // when the user is back to e-signature step
    if (event.selectedIndex === 0) this.restrictToGeneralStep();
  }

  /**
   * executes at last of the form submission
   */
  onSubmit(): void {
    const formValue = {
      general: this.generalForm.value,
      questionnaire: this.questionnaireForm.value,
      employementHistory: this.employementHistoryForm.value
        .employeeHistoryFormArray,
      reference: this.referenceForm.value.referenceFormArray,
      accountInformation: this.accountInformationForm.value,
      applicantStatement: this.applicantStatementForm.value
    };
    console.log(formValue);
    // need to call API
  }

  /**
   * executes on Next button click of each stepper
   * @param form
   * @param nextStepIndex
   */
  onFormSubmit(form: FormGroup, nextStepIndex: number): void {
    if (form.invalid) return;
    // && (nextStepIndex != 4 && nextStepIndex != 5)

    if (nextStepIndex != 7) {
      this.stepper.selected.completed = true;
      this.stepper.selectedIndex = nextStepIndex;
    }

    switch (nextStepIndex) {
      case 2:
        this.generalForm = form;
        break;
      case 3:
        this.questionnaireForm = form;
        break;
      case 4:
        this.employementHistoryForm = form;
        break;
      case 5:
        this.referenceForm = form;
        break;
      case 6:
        this.accountInformationForm = form;
        break;
      case 7:
        this.applicantStatementForm = form;
        this.onSubmit();
        break;
    }
  }

  goBackToPreviousStep(previousStepIndex: number): void {
    this.stepper.selectedIndex = previousStepIndex;
  }

  gotoLoginPage() {
    this.router.navigate(["login"]);
  }
}
