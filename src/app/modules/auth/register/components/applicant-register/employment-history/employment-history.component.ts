import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FormGroup, Validators, FormArray, FormBuilder } from "@angular/forms";
import { ValidationMessageService } from "@app/shared/services/validation-message/validation-message.service";

@Component({
  selector: "simpliflysaas-employment-history",
  templateUrl: "./employment-history.component.html",
  styleUrls: ["./employment-history.component.scss"]
})
export class EmploymentHistoryComponent implements OnInit {
  @Output() employeeHistoryFormSubmit: EventEmitter<any> = new EventEmitter();
  @Output() onBack: EventEmitter<any> = new EventEmitter();

  employeeHistoryForm: FormGroup;
  submitted: boolean;

  constructor(
    private fb: FormBuilder,
    private validationMessageService: ValidationMessageService
  ) {}

  ngOnInit() {
    this.buildEmployeeHistoryForm();
  }

  buildEmployeeHistoryForm(): void {
    this.employeeHistoryForm = this.fb.group({
      employeeHistoryFormArray: this.fb.array([this.employeeHistoryFormGroup()])
    });
  }

  /**
   * employee form group
   */
  employeeHistoryFormGroup(): FormGroup {
    return this.fb.group({
      companyName: ["", [Validators.required]],
      title: ["", Validators.required],
      startDate: [],
      employmentType: [],
      endDate: [],
      cellPhone: [],
      city: [],
      state: [],
      zip: [],
      dutites: [],
      reasonForLeaving: []
    });
  }

  /**
   * Executes on click of Add button
   */
  addEmployeeHistory(): void {
    this.submitted = true;
    this.scrollToErrorField();
    if (this.employeeHistoryForm.invalid) return;

    (<FormArray>this.employeeHistoryForm.get("employeeHistoryFormArray")).push(
      this.employeeHistoryFormGroup()
    );
    this.submitted = false;
  }

  /**
   * get employee history form array
   */
  get formArray(): FormArray {
    return this.employeeHistoryForm.get(
      "employeeHistoryFormArray"
    ) as FormArray;
  }

  /**
   * Delete Particular Employee
   * @param index
   */
  deleteEmployeeHistory(index: number): void {
    this.formArray.removeAt(index);
  }

  onSubmit(): void {
    this.submitted = true;
    this.scrollToErrorField();
    this.employeeHistoryFormSubmit.emit(this.employeeHistoryForm);
  }

  goBack(): void {
    this.onBack.emit(true);
  }

  scrollToErrorField() {
    setTimeout(() => {
      this.validationMessageService.scrollToErrorField(
        document.querySelectorAll(".error-msg")
      );
    }, 100);
  }
}
