import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RegisterRoutingModule } from "./register-routing.module";
import { RegisterComponent } from "./components/register.component";
import { ApplicantRegisterComponent } from "./components/applicant-register/applicant-register.component";
import { SharedModule } from "@app/shared/shared.module";
import { MatFormFieldModule, MatInputModule } from "@angular/material";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { HttpClient } from "@angular/common/http";
import { GeneralComponent } from "./components/applicant-register/general/general.component";
import { QuestionnaireComponent } from "./components/applicant-register/questionnaire/questionnaire.component";
import { EmploymentHistoryComponent } from "./components/applicant-register/employment-history/employment-history.component";
import { ReferencesComponent } from "./components/applicant-register/references/references.component";
import { AccountInformationComponent } from "./components/applicant-register/account-information/account-information.component";
import { ApplicantStatementComponent } from "./components/applicant-register/applicant-statement/applicant-statement.component";
import { ReactiveFormsModule } from "@angular/forms";

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/auth/register/", ".json");
}

@NgModule({
  declarations: [
    RegisterComponent,
    ApplicantRegisterComponent,
    GeneralComponent,
    QuestionnaireComponent,
    EmploymentHistoryComponent,
    ReferencesComponent,
    AccountInformationComponent,
    ApplicantStatementComponent
  ],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    SharedModule,
    MatFormFieldModule,
    MatInputModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      },
      isolate: true
    }),
    ReactiveFormsModule
  ]
})
export class RegisterModule {}
