import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { Utility } from "@app/modules/utility/models/utility.model";
import { SessionStorageService } from "@app/shared/services/session-storage/session-storage.service";
import { StorageConst } from "@app/shared/constants/storage.constant";

@Component({
  selector: "simpliflysaas-utility-list",
  templateUrl: "./utility-list.component.html",
  styleUrls: ["./utility-list.component.scss"]
})
export class UtilityListComponent implements OnInit {
  @Input() utilityList: Utility[];
  @Input() totalUtilities: number;
  @Input() loading: boolean;

  utilitySearch: string;

  // pagination
  itemsPerPage: number = 10;
  currentPage: number = 1;

  // session storage constant
  sessionStorageConst = StorageConst.SESSION_STORAGE;

  constructor(
    private router: Router,
    private sessionStorageService: SessionStorageService
  ) {}

  ngOnInit() {
    const currentPageFromStorage = this.sessionStorageService.getSessionStorageItem(
      this.sessionStorageConst.UTILITIES_CURRENT_PAGE
    );
    this.currentPage = currentPageFromStorage;
  }

  gotoSubUtilityPage(utility: Utility): void {
    this.router.navigate(["/utilities/", utility.path]);
  }

  setCurrentPage(pageNumber): void {
    this.currentPage = pageNumber;
  }

  // sorting logic
  key = "title"; // sort default by name
  reverse = false;
  sortList(key): void {
    this.key = key;
    this.reverse = !this.reverse;
    this.setCurrentPage(1);
  }
}
