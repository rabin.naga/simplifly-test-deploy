import { Component, OnInit, TemplateRef } from "@angular/core";
import { UtilityService } from "@app/modules/utility/services/utility.service";
import { Utility } from "@app/modules/utility/models/utility.model";
import { CustomResponse } from "@app/shared/models/custom-response.model";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";

@Component({
  selector: "simpliflysaas-utility",
  templateUrl: "./utility.component.html",
  styleUrls: ["./utility.component.scss"]
})
export class UtilityComponent implements OnInit {
  modalRef: BsModalRef;
  utilityList: Utility[];
  totalUtilitiesCount: number;
  utilityForm: FormGroup;

  utilitiesLoading: boolean;
  //form validation
  submitted: boolean;
  get title() {
    return this.utilityForm.controls["title"];
  }
  get tableName() {
    return this.utilityForm.controls["table_name"];
  }
  get path() {
    return this.utilityForm.controls["path"];
  }

  constructor(
    private utilityService: UtilityService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private toastrMessageService: ToastrMessageService
  ) {}

  ngOnInit() {
    this.getUtilityList();
  }

  /**
   * This method is to retreive list of utilites to be passed to Utility-list Component
   */
  getUtilityList(): void {
    this.utilitiesLoading = true;
    this.utilityService.getUtilityList().subscribe(
      (response: CustomResponse) => {
        if (response && response.status) {
          this.utilityList = response.data;
          this.totalUtilitiesCount = response.count;
        }
        this.utilitiesLoading = false;
      },
      error => {
        console.log(error);
        this.utilitiesLoading = false;
      }
    );
  }

  /**
   * This method executes when the user clicks on Add
   * @param template
   */
  openModal(template: TemplateRef<any>): void {
    this.buildUtilityForm();
    this.modalRef = this.modalService.show(template);
  }

  buildUtilityForm(): void {
    this.utilityForm = this.formBuilder.group({
      title: ["", Validators.required],
      table_name: ["", Validators.required],
      path: ["", Validators.required]
    });
  }

  onSubmitUtility(): void {
    this.submitted = true;

    if (this.utilityForm.invalid) return;

    this.addUtility();
    this.modalRef.hide();
  }

  addUtility(): void {
    console.log(this.utilityForm.value);
    this.utilityService.addUtility(this.utilityForm.value).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
          return;
        }
        this.toastrMessageService.showError(response.data);
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getUtilityList();
      }
    );
  }
}
