import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubUtilityComponent } from './sub-utility.component';

describe('SubUtilityComponent', () => {
  let component: SubUtilityComponent;
  let fixture: ComponentFixture<SubUtilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubUtilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubUtilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
