import { Component, OnInit, TemplateRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { TextboxQuestion } from "../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-textbox";
import { UtilityService } from "@app/modules/utility/services/utility.service";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { PayGroup } from "@app/modules/utility/models/pay-group.model";

@Component({
  selector: "simpliflysaas-pay-group",
  templateUrl: "./pay-group.component.html",
  styleUrls: ["./pay-group.component.scss"]
})
export class PayGroupComponent implements OnInit {
  payGroupForm: FormGroup;
  payGroupLoading: boolean;
  payGroup: any;
  submitted: boolean;
  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  // data table
  dataPerPage: number = 5;
  processingAction: boolean;
  payGroupColumns = [
    new TextboxQuestion({
      key: "pay_group",
      label: "Pay Group",
      required: true,
      order: 1
    })
  ];
  constructor(
    private utilityService: UtilityService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private toastrMessageService: ToastrMessageService
  ) {}

  ngOnInit() {
    this.payGroupLoading = true;
    this.getPayGroup();
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, this.config);
    this.closeEditMode();
    this.buildPayGroupForm();
  }

  closeModal(): void {
    // setting it to true again to trigger ngOnChanges for closeEditMode for the next edit item
    this.showSaveOption = true;
    this.submitted = false;
    this.modalRef.hide();
  }

  buildPayGroupForm(): void {
    this.payGroupForm = this.formBuilder.group({
      pay_group: ["", Validators.required]
    });
  }

  getPayGroup(): void {
    this.utilityService.getPayGroup().subscribe(
      (response: any) => {
        if (response && response.status) {
          this.payGroup = response.data;
        } else {
          this.payGroup = [];
        }
      },
      error => {
        this.payGroupLoading = false;
      },
      () => {
        this.payGroupLoading = false;
      }
    );
  }

  /**
   * Method to add Pay Group
   */
  addPayGroup(): void {
    this.submitted = true;

    if (this.payGroupForm.invalid) return;

    this.utilityService.addPayGroup(this.payGroupForm.value).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
          return;
        }

        //preview server side validation messages
        this.toastrMessageService.showError(response.data);
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.closeModal();
        this.getPayGroup();
      }
    );
  }

  /**
   * Method to update Pay Group
   */
  updatePayGroup(payGroup) {
    const id = payGroup.original_data.id;
    delete payGroup.original_data;

    this.processingAction = true;
    this.utilityService.updatePayGroup(id, payGroup).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getPayGroup();
        this.processingAction = false;
      }
    );
  }

  /**
   * Method to delete Pay Group
   */
  deletePayGroup(payGroup: PayGroup) {
    this.processingAction = true;
    this.utilityService.deletePayGroup(payGroup.id).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.closeEditMode();
        this.getPayGroup();
        this.processingAction = false;
      }
    );
  }

  /**
   * This method executes on click of delete Pay Group
   * @param payGroup
   */
  openConfirmDialog(payGroup: PayGroup): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.action = "delete";
    this.modalRef.content.data = payGroup.pay_group;
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deletePayGroup(payGroup);
      }
    });
  }

  /**
   * This method executes when the user clicks cancel during edit phase.
   */
  showSaveOption: boolean;
  closeEditMode(): void {
    this.showSaveOption = false;
  }

  // form control names
  get pay_group() {
    return this.payGroupForm.get("pay_group");
  }
}
