import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonSubUtilityComponent } from './common-sub-utility.component';

describe('CommonSubUtilityComponent', () => {
  let component: CommonSubUtilityComponent;
  let fixture: ComponentFixture<CommonSubUtilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonSubUtilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonSubUtilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
