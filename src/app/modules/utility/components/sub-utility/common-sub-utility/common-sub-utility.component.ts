import { Component, OnInit, TemplateRef, Input } from "@angular/core";
import { UtilityService } from "@app/modules/utility/services/utility.service";
import { CustomResponse } from "@app/shared/models/custom-response.model";
import { FormGroup } from "@angular/forms";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap";
import { TextboxQuestion } from "../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-textbox";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { DynamicFormService } from "@app/shared/components/dynamic-form/services/dynamic-form.service";

@Component({
  selector: "simpliflysaas-common-sub-utility",
  templateUrl: "./common-sub-utility.component.html",
  styleUrls: ["./common-sub-utility.component.scss"]
})
export class CommonSubUtilityComponent implements OnInit {
  @Input() selectedUtility: string;

  itemList: any;

  // params for sub utility list
  onSubUtilityItemAdd: boolean;
  onOpenOfAddSubUtilityModal: boolean;

  // form
  submitted: boolean;

  // confirmation modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  constructor(
    private utilityService: UtilityService,
    private toastrMessageService: ToastrMessageService,
    private modalService: BsModalService,
    private dynamicFormService: DynamicFormService
  ) {}

  ngOnInit() {
    this.commonSubUtilityLoading = true;
    this.getSubUtilityList();
  }

  buildSubUtilityForm(): void {
    this.commonSubUtilityForm = this.dynamicFormService.createFormGroup(
      this.commonSubUtilityFormControls
    );
  }

  // dynamic form
  commonSubUtilityForm: FormGroup;

  // data table
  commonSubUtilityLoading: boolean;
  commonSubUtility: any;
  dataPerPage: number = 5;
  commonSubUtilityFormControls = [
    new TextboxQuestion({
      key: "title",
      label: "Title",
      required: true,
      order: 1
    })
  ];

  getSubUtilityList(): void {
    this.utilityService.getSelectedUtilityList(this.selectedUtility).subscribe(
      (response: any) => {
        if (response && response.status) {
          this.commonSubUtility = response.data;
        } else {
          this.commonSubUtility = [];
        }
      },
      error => {
        this.commonSubUtilityLoading = false;
      },
      () => {
        this.commonSubUtilityLoading = false;
      }
    );
  }

  addSubUtilityItem(): void {
    this.submitted = true;

    if (this.commonSubUtilityForm.invalid) return;

    const requestPayload = {
      title: this.commonSubUtilityForm.controls["title"].value
    };
    this.utilityService
      .addSubUtilityItem(this.selectedUtility, requestPayload)
      .subscribe(
        (response: CustomResponse) => {
          if (response && response.status) {
            this.toastrMessageService.showSuccess(response.data);
          }
        },
        error => {
          this.toastrMessageService.showError(error);
        },
        () => {
          this.submitted = false;
          this.modalRef.hide();
          this.getSubUtilityList();
        }
      );
  }

  /**
   * Method to update
   */
  processingAction: boolean;
  updateSubUtility(item) {
    const id = item.original_data.id;
    delete item.original_data;

    this.processingAction = true;
    this.utilityService
      .updateSelectedUtilityItem(this.selectedUtility, id, item)
      .subscribe(
        response => {
          if (response && response.status) {
            this.toastrMessageService.showSuccess(response.data);
          }
        },
        error => {
          this.toastrMessageService.showError(error);
        },
        () => {
          this.getSubUtilityList();
          this.processingAction = false;
        }
      );
  }

  /**
   * Method to delete
   */
  deleteSubUtilityItem(subUtilityItem) {
    this.processingAction = true;
    this.utilityService
      .deleteSelectedUtilityItem(this.selectedUtility, subUtilityItem.id)
      .subscribe(
        response => {
          if (response && response.status) {
            this.toastrMessageService.showSuccess(response.data);
          }
        },
        error => {
          this.toastrMessageService.showError(error);
        },
        () => {
          this.getSubUtilityList();
          this.processingAction = false;
        }
      );
  }

  /**
   * This method opens the confirm modal on clicking delete
   * @param subUtilityItem
   */
  openConfirmDialog(subUtilityItem): void {
    // if (this.disableEditAndDeleteBtn) return;

    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.action = "delete";
    this.modalRef.content.data = subUtilityItem.title;
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deleteSubUtilityItem(subUtilityItem);
      }
    });
  }

  openModal(template: TemplateRef<any>): void {
    this.closeEditMode();
    // this.onOpenOfAddSubUtilityModal = true;
    this.modalRef = this.modalService.show(template, this.config);
    this.buildSubUtilityForm();

    //reset the value
    this.onSubUtilityItemAdd = false;
  }

  closeModal() {
    // this.onOpenOfAddSubUtilityModal = false;
    this.submitted = false;
    this.modalRef.hide();
  }

  /**
   * This method executes when the user clicks cancel during edit phase.
   */
  showSaveOption: boolean;
  closeEditMode(): void {
    this.showSaveOption = false;
  }
}
