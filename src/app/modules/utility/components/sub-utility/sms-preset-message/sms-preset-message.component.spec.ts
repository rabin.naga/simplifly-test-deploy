import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsPresetMessageComponent } from './sms-preset-message.component';

describe('SmsPresetMessageComponent', () => {
  let component: SmsPresetMessageComponent;
  let fixture: ComponentFixture<SmsPresetMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsPresetMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsPresetMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
