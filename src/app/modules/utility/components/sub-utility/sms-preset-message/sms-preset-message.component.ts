import { Component, OnInit, TemplateRef } from "@angular/core";
import { SmsPresetMessage } from "@app/modules/utility/models/sms-preset-message.model";
import { FormGroup } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { TextboxQuestion } from "../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-textbox";
import { UtilityService } from "@app/modules/utility/services/utility.service";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { DynamicFormService } from "@app/shared/components/dynamic-form/services/dynamic-form.service";
import { DropdownQuestion } from "../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-dropdown";

@Component({
  selector: "simpliflysaas-sms-preset-message",
  templateUrl: "./sms-preset-message.component.html",
  styleUrls: ["./sms-preset-message.component.scss"]
})
export class SmsPresetMessageComponent implements OnInit {
  smsPresetMessageForm: FormGroup;
  smsPresetMessageLoading: boolean;
  smsPresetMessage: SmsPresetMessage[];
  submitted: boolean;

  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  // table column
  dataPerPage: number = 5;
  processingAction: boolean;
  smsPresetMessageFormGroup = [
    new TextboxQuestion({
      key: "message",
      label: "Message",
      required: true,
      minLength: 5,
      // maxLength: 10,
      // pattern: /^([0-9])*$/,
      order: 1
    }),

    new DropdownQuestion({
      key: "category",
      label: "Category",
      required: true,
      options: [
        { key: 1, value: "Category One" },
        { key: 2, value: "Category Two" },
        { key: 3, value: "Category Three" }
      ],
      order: 2
    })
  ];

  constructor(
    private utilityService: UtilityService,
    private modalService: BsModalService,
    private toastrMessageService: ToastrMessageService,
    private dynamicFormService: DynamicFormService
  ) {}

  ngOnInit() {
    this.smsPresetMessageLoading = true;
    this.getSmsPresetMessage();
  }

  openModel(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, this.config);
    this.closeEditMode();
    this.buildSmsPresetMessageForm();
  }

  closeModal(): void {
    // setting it to true again to trigger ngOnChanges for closeEditMode for the next edit item
    this.showSaveOption = true;
    this.submitted = false;
    this.modalRef.hide();
  }

  buildSmsPresetMessageForm() {
    this.smsPresetMessageForm = this.dynamicFormService.createFormGroup(
      this.smsPresetMessageFormGroup
    );
  }

  /**
   * Method to get Pay Type
   */
  getSmsPresetMessage(): void {
    this.utilityService.getSmsPresetMessage().subscribe(
      (response: any) => {
        if (response && response.status) {
          this.smsPresetMessage = response.data;
        }
      },
      error => {
        this.smsPresetMessageLoading = false;
      },
      () => {
        this.smsPresetMessageLoading = false;
      }
    );
  }

  /**
   * Method to add Pay Type
   */
  addSmsPresetMessage(): void {
    this.submitted = true;

    if (this.smsPresetMessageForm.invalid) return;

    this.utilityService
      .addSmsPresetMessage(this.smsPresetMessageForm.value)
      .subscribe(
        response => {
          if (response && response.status) {
            this.toastrMessageService.showSuccess(response.data);
            return;
          }

          //preview server side validation messages
          this.toastrMessageService.showError(response.data);
        },
        error => {
          this.toastrMessageService.showError(error);
        },
        () => {
          this.closeModal();
          this.getSmsPresetMessage();
        }
      );
  }

  /**
   * Method to update Pay Type
   */
  updateSmsPresetMessage(smsPresetMessage: SmsPresetMessage) {
    const id = smsPresetMessage.original_data.id;
    delete smsPresetMessage.original_data;

    this.processingAction = true;
    this.utilityService.updateSmsPresetMessage(id, smsPresetMessage).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getSmsPresetMessage();
        this.processingAction = false;
      }
    );
  }

  /**
   * Method to delete Pay Type
   */
  deleteSmsPresetMessage(smsPresetMessage: SmsPresetMessage) {
    this.processingAction = true;
    this.utilityService.deleteSmsPresetMessage(smsPresetMessage.id).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.closeEditMode();
        this.getSmsPresetMessage();
        this.processingAction = false;
      }
    );
  }

  /**
   * This method executes on click of delete Pay Type
   * @param smsPresetMessage
   */
  openConfirmDialog(smsPresetMessage: SmsPresetMessage): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.action = "delete";
    this.modalRef.content.data = smsPresetMessage.message;
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deleteSmsPresetMessage(smsPresetMessage);
      }
    });
  }

  /**
   * This method executes when the user clicks cancel during edit phase.
   */
  showSaveOption: boolean;
  closeEditMode(): void {
    this.showSaveOption = false;
  }
}
