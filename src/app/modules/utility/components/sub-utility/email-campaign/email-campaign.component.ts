import { Component, OnInit, TemplateRef } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { TextboxQuestion } from "../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-textbox";
import { UtilityService } from "@app/modules/utility/services/utility.service";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { DynamicFormService } from "@app/shared/components/dynamic-form/services/dynamic-form.service";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { EmailCampaign } from "@app/modules/utility/models/email-campaign.model";

@Component({
  selector: "simpliflysaas-email-campaign",
  templateUrl: "./email-campaign.component.html",
  styleUrls: ["./email-campaign.component.scss"]
})
export class EmailCampaignComponent implements OnInit {
  emailCampaignForm: FormGroup;
  emailCampaignLoading: boolean;
  emailCampaign: any;
  submitted: boolean;

  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  // table column
  dataPerPage: number = 5;
  processingAction: boolean;
  emailCampaignColumns = [
    new TextboxQuestion({
      key: "campaign_type",
      label: "Campaign Type",
      required: true,
      order: 1
    })
  ];

  constructor(
    private utilityService: UtilityService,
    private modalService: BsModalService,
    private toastrMessageService: ToastrMessageService,
    private dynamicFormService: DynamicFormService
  ) {}

  ngOnInit() {
    this.emailCampaignLoading = true;
    this.getEmailCampaign();
  }

  openModel(template: TemplateRef<any>): void {
    this.closeEditMode();
    this.modalRef = this.modalService.show(template, this.config);
    this.buildEmailCampaignForm();
  }

  closeModal(): void {
    // setting it to true again to trigger ngOnChanges for closeEditMode for the next edit item
    this.submitted = false;
    this.modalRef.hide();
  }

  buildEmailCampaignForm(): void {
    this.emailCampaignForm = this.dynamicFormService.createFormGroup(
      this.emailCampaignColumns
    );
  }

  /**
   * Method to get Email Campaign
   */
  getEmailCampaign(): void {
    this.utilityService.getEmailCampaign().subscribe(
      (response: any) => {
        if (response && response.status) {
          this.emailCampaign = response.data;
        } else {
          this.emailCampaign = [];
        }
      },
      error => {
        console.log(error);
        this.emailCampaignLoading = false;
      },
      () => {
        this.emailCampaignLoading = false;
      }
    );
  }

  /**
   * Method to add Email Campaign
   */
  addEmailCampaign(): void {
    this.submitted = true;

    if (this.emailCampaignForm.invalid) return;

    this.utilityService
      .addEmailCampaign(this.emailCampaignForm.value)
      .subscribe(
        response => {
          if (response && response.status) {
            this.toastrMessageService.showSuccess(response.data);
            return;
          }

          //preview server side validation messages
          this.toastrMessageService.showError(response.data);
        },
        error => {
          this.toastrMessageService.showError(error);
        },
        () => {
          this.closeModal();
          this.getEmailCampaign();
        }
      );
  }

  /**
   * Method to update Email Campaign
   */
  updateEmailCampaign(emailCampaign) {
    const id = emailCampaign.original_data.id;
    delete emailCampaign.original_data;

    this.processingAction = true;
    this.utilityService.updateEmailCampaign(id, emailCampaign).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getEmailCampaign();
        this.processingAction = false;
      }
    );
  }

  /**
   * Method to delete Email Campaign
   */
  deleteEmailCampaign(emailCampaign: EmailCampaign) {
    this.processingAction = true;
    this.utilityService.deleteEmailCampaign(emailCampaign.id).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getEmailCampaign();
        this.processingAction = false;
      }
    );
  }

  /**
   * This method executes on click of delete Email Campaign
   * @param emailCampaign
   */
  openConfirmDialog(emailCampaign: EmailCampaign): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.action = "delete";
    this.modalRef.content.data = emailCampaign.campaign_type;
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deleteEmailCampaign(emailCampaign);
      }
    });
  }

  /**
   * This method executes when the user clicks cancel during edit phase.
   */
  showSaveOption: boolean;
  closeEditMode(): void {
    this.showSaveOption = false;
  }
}
