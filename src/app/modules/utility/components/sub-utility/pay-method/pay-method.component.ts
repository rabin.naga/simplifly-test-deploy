import { Component, OnInit, TemplateRef } from "@angular/core";
import { UtilityService } from "@app/modules/utility/services/utility.service";
import { TextboxQuestion } from "../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-textbox";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { PayMethod } from "@app/modules/utility/models/pay-method.model";

@Component({
  selector: "simpliflysaas-pay-method",
  templateUrl: "./pay-method.component.html",
  styleUrls: ["./pay-method.component.scss"]
})
export class PayMethodComponent implements OnInit {
  payMethodForm: FormGroup;
  payMethodLoading: boolean;
  payMethod: any;
  submitted: boolean;
  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  // data table
  dataPerPage: number = 5;
  processingAction: boolean;
  payMethodColumns = [
    new TextboxQuestion({
      key: "pay_method",
      label: "Pay Method",
      required: true,
      order: 1
    })
  ];
  constructor(
    private utilityService: UtilityService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private toastrMessageService: ToastrMessageService
  ) {}

  ngOnInit() {
    this.payMethodLoading = true;
    this.getPayMethod();
  }

  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, this.config);
    this.closeEditMode();
    this.buildPayMethodForm();
  }

  closeModal(): void {
    // setting it to true again to trigger ngOnChanges for closeEditMode for the next edit item
    this.showSaveOption = true;
    this.submitted = false;
    this.modalRef.hide();
  }

  buildPayMethodForm(): void {
    this.payMethodForm = this.formBuilder.group({
      pay_method: ["", Validators.required]
    });
  }

  getPayMethod(): void {
    this.utilityService.getPayMethod().subscribe(
      (response: any) => {
        if (response && response.status) {
          this.payMethod = response.data;
        } else {
          this.payMethod = [];
        }
      },
      error => {
        console.log(error);
        this.payMethodLoading = false;
      },
      () => {
        this.payMethodLoading = false;
      }
    );
  }

  /**
   * Method to add Pay Method
   */
  addPayMethod(): void {
    this.submitted = true;

    if (this.payMethodForm.invalid) return;

    this.utilityService.addPayMethod(this.payMethodForm.value).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
          return;
        }

        //preview server side validation messages
        this.toastrMessageService.showError(response.data);
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.closeModal();
        this.getPayMethod();
      }
    );
  }

  /**
   * Method to call update API for Pay Method
   */
  updatePayMethod(payMethod) {
    const id = payMethod.original_data.id;
    delete payMethod.original_data;

    this.processingAction = true;
    this.utilityService.updatePayMethod(id, payMethod).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getPayMethod();
        this.processingAction = false;
      }
    );
  }

  /**
   * Method to delete Pay Period
   */
  deletePayMethod(payMethod: PayMethod) {
    this.processingAction = true;
    this.utilityService.deletePayMethod(payMethod.id).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.closeEditMode();
        this.getPayMethod();
        this.processingAction = false;
      }
    );
  }

  /**
   * This method executes on click of delete Pay Method
   * @param payMethod
   */
  openConfirmDialog(payMethod: PayMethod): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.action = "delete";
    this.modalRef.content.data = payMethod.pay_method;
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deletePayMethod(payMethod);
      }
    });
  }

  /**
   * This method executes when the user clicks cancel during edit phase.
   */
  showSaveOption: boolean;
  closeEditMode(): void {
    this.showSaveOption = false;
  }

  // form control names
  get pay_method() {
    return this.payMethodForm.get("pay_method");
  }
}
