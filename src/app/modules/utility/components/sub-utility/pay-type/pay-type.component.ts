import { Component, OnInit, TemplateRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { TextboxQuestion } from "../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-textbox";
import { UtilityService } from "@app/modules/utility/services/utility.service";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { PayType } from "@app/modules/utility/models/pay-type.model";
import { DynamicFormService } from "@app/shared/components/dynamic-form/services/dynamic-form.service";

@Component({
  selector: "simpliflysaas-pay-type",
  templateUrl: "./pay-type.component.html",
  styleUrls: ["./pay-type.component.scss"]
})
export class PayTypeComponent implements OnInit {
  payTypeForm: FormGroup;
  payTypeLoading: boolean;
  payType: any;
  submitted: boolean;

  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  // table column
  dataPerPage: number = 5;
  processingAction: boolean;
  payTypeColumns = [
    new TextboxQuestion({
      key: "pay_type",
      label: "Pay Type",
      required: true,
      order: 1
    })
  ];

  constructor(
    private utilityService: UtilityService,
    private modalService: BsModalService,
    private toastrMessageService: ToastrMessageService,
    private dynamicFormService: DynamicFormService
  ) {}

  ngOnInit() {
    this.payTypeLoading = true;
    this.getPayType();
  }

  openModel(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, this.config);
    this.closeEditMode();
    this.buildPayTypeForm();
  }

  closeModal(): void {
    // setting it to true again to trigger ngOnChanges for closeEditMode for the next edit item
    this.showSaveOption = true;
    this.submitted = false;
    this.modalRef.hide();
  }

  buildPayTypeForm(): void {
    this.payTypeForm = this.dynamicFormService.createFormGroup(
      this.payTypeColumns
    );
  }

  /**
   * Method to get Pay Type
   */
  getPayType(): void {
    this.utilityService.getPayType().subscribe(
      (response: any) => {
        if (response && response.status) {
          this.payType = response.data;
        } else {
          this.payType = [];
        }
      },
      error => {
        this.payTypeLoading = false;
      },
      () => {
        this.payTypeLoading = false;
      }
    );
  }

  /**
   * Method to add Pay Type
   */
  addPayType(): void {
    this.submitted = true;

    if (this.payTypeForm.invalid) return;

    this.utilityService.addPayType(this.payTypeForm.value).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
          return;
        }

        //preview server side validation messages
        this.toastrMessageService.showError(response.data);
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.closeModal();
        this.getPayType();
      }
    );
  }

  /**
   * Method to update Pay Type
   */
  updatePayType(payType) {
    const id = payType.original_data.id;
    delete payType.original_data;

    this.processingAction = true;
    this.utilityService.updatePayType(id, payType).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getPayType();
        this.processingAction = false;
      }
    );
  }

  /**
   * Method to delete Pay Type
   */
  deletePayType(payType: PayType) {
    this.processingAction = true;
    this.utilityService.deletePayType(payType.id).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.closeEditMode();
        this.getPayType();
        this.processingAction = false;
      }
    );
  }

  /**
   * This method executes on click of delete Pay Type
   * @param payType
   */
  openConfirmDialog(payType: PayType): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.action = "delete";
    this.modalRef.content.data = payType.pay_type;
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deletePayType(payType);
      }
    });
  }

  /**
   * This method executes when the user clicks cancel during edit phase.
   */
  showSaveOption: boolean;
  closeEditMode(): void {
    this.showSaveOption = false;
  }
}
