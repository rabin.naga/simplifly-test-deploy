import { Component, OnInit, TemplateRef } from "@angular/core";
import { TextboxQuestion } from "../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-textbox";
import { UtilityService } from "@app/modules/utility/services/utility.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { Visit } from "./../../../models/visit.model";

@Component({
  selector: "simpliflysaas-visit",
  templateUrl: "./visit.component.html",
  styleUrls: ["./visit.component.scss"]
})
export class VisitComponent implements OnInit {
  visitForm: FormGroup;
  visitLoading: boolean;
  visit: Visit[];
  submitted: boolean;

  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  // table column
  dataPerPage: number = 5;
  processingAction: boolean;
  visitColumns = [
    new TextboxQuestion({
      key: "visit_type",
      label: "Visit Type",
      required: true,
      order: 1
    })
  ];

  constructor(
    private utilityService: UtilityService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private toastrMessageService: ToastrMessageService
  ) {}

  ngOnInit() {
    this.visitLoading = true;
    this.getVisit();
  }

  /**
   * Method to get visit type
   */
  getVisit(): void {
    this.utilityService.getVisitType().subscribe(
      (response: any) => {
        if (response && response.status) {
          this.visit = response.data;
        } else {
          this.visit = [];
        }
      },
      error => {
        console.log(error);
        this.visitLoading = false;
      },
      () => {
        this.visitLoading = false;
      }
    );
  }

  openModel(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, this.config);
    this.closeEditMode();
    this.buildVisitForm();
  }

  closeModal(): void {
    // setting it to true again to trigger ngOnChanges for closeEditMode for the next edit item
    this.showSaveOption = true;
    this.submitted = false;
    this.modalRef.hide();
  }

  buildVisitForm(): void {
    this.visitForm = this.formBuilder.group({
      visit_type: ["", Validators.required]
    });
  }

  /**
   * Method to add visit type
   */
  addVisit(): void {
    this.submitted = true;

    if (this.visitForm.invalid) return;

    this.utilityService.addVisitType(this.visitForm.value).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
          return;
        }

        //preview server side validation messages
        this.toastrMessageService.showError(response.data);
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.closeModal();
        this.getVisit();
      }
    );
  }

  /**
   * Method to update visit type
   */
  updateVisit(visit: Visit) {
    const id = visit.original_data.id;
    delete visit.original_data;

    this.processingAction = true;
    this.utilityService.updateVisitType(id, visit).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getVisit();
        this.processingAction = false;
      }
    );
  }

  /**
   * Method to delete visit type
   */
  deleteVisit(visit: Visit) {
    this.processingAction = true;
    this.utilityService.deleteVisitType(visit.id).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.closeEditMode();
        this.getVisit();
        this.processingAction = false;
      }
    );
  }

  /**
   * This method executes on click of delete Pay Period
   * @param visit
   */
  openConfirmDialog(visit: Visit): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.action = "delete";
    this.modalRef.content.data = visit.visit_type;
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deleteVisit(visit);
      }
    });
  }

  /**
   * This method executes when the user clicks cancel during edit phase.
   */
  showSaveOption: boolean;
  closeEditMode(): void {
    this.showSaveOption = false;
  }

  // form control names
  get visitFormControl() {
    return this.visitForm.get("visit_type");
  }
}
