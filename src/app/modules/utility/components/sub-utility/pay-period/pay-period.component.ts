import { Component, OnInit, TemplateRef } from "@angular/core";
import { TextboxQuestion } from "../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-textbox";
import { UtilityService } from "@app/modules/utility/services/utility.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { PayPeriod } from "@app/modules/utility/models/pay-period.model";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";

@Component({
  selector: "simpliflysaas-pay-period",
  templateUrl: "./pay-period.component.html",
  styleUrls: ["./pay-period.component.scss"]
})
export class PayPeriodComponent implements OnInit {
  payPeriodForm: FormGroup;
  payPeriodLoading: boolean;
  payPeriod: any;
  submitted: boolean;

  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  // table column
  dataPerPage: number = 5;
  processingAction: boolean;
  payPeriodColumns = [
    new TextboxQuestion({
      key: "pay_period",
      label: "Pay Period",
      required: true,
      order: 1
    })
  ];

  constructor(
    private utilityService: UtilityService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private toastrMessageService: ToastrMessageService
  ) {}

  ngOnInit() {
    this.payPeriodLoading = true;
    this.getPayPeriod();
  }

  openModel(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, this.config);
    this.closeEditMode();
    this.buildPayPeriodForm();
  }

  closeModal(): void {
    // setting it to true again to trigger ngOnChanges for closeEditMode for the next edit item
    this.showSaveOption = true;
    this.submitted = false;
    this.modalRef.hide();
  }

  buildPayPeriodForm(): void {
    this.payPeriodForm = this.formBuilder.group({
      pay_period: ["", Validators.required]
    });
  }

  /**
   * Method to get Pay Period
   */
  getPayPeriod(): void {
    this.utilityService.getPayPeriod().subscribe(
      (response: any) => {
        if (response && response.status) {
          this.payPeriod = response.data;
        } else {
          this.payPeriod = [];
        }
      },
      error => {
        console.log(error);
        this.payPeriodLoading = false;
      },
      () => {
        this.payPeriodLoading = false;
      }
    );
  }

  /**
   * Method to add Pay Period
   */
  addPayPeriod(): void {
    this.submitted = true;

    if (this.payPeriodForm.invalid) return;

    this.utilityService.addPayPeriod(this.payPeriodForm.value).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
          return;
        }

        //preview server side validation messages
        this.toastrMessageService.showError(response.data);
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.closeModal();
        this.getPayPeriod();
      }
    );
  }

  /**
   * Method to update Pay Period
   */
  updatePayPeriod(payPeriod) {
    const id = payPeriod.original_data.id;
    delete payPeriod.original_data;

    this.processingAction = true;
    this.utilityService.updatePayPeriod(id, payPeriod).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getPayPeriod();
        this.processingAction = false;
      }
    );
  }

  /**
   * Method to delete Pay Period
   */
  deletePayPeriod(payPeriod: PayPeriod) {
    this.processingAction = true;
    this.utilityService.deletePayPeriod(payPeriod.id).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.closeEditMode();
        this.getPayPeriod();
        this.processingAction = false;
      }
    );
  }

  /**
   * This method executes on click of delete Pay Period
   * @param payPeriod
   */
  openConfirmDialog(payPeriod: PayPeriod): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.action = "delete";
    this.modalRef.content.data = payPeriod.pay_period;
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deletePayPeriod(payPeriod);
      }
    });
  }

  /**
   * This method executes when the user clicks cancel during edit phase.
   */
  showSaveOption: boolean;
  closeEditMode(): void {
    this.showSaveOption = false;
  }

  // form control names
  get pay_period() {
    return this.payPeriodForm.get("pay_period");
  }
}
