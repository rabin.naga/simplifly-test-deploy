import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeIncomeCodeComponent } from './employee-income-code.component';

describe('EmployeeIncomeCodeComponent', () => {
  let component: EmployeeIncomeCodeComponent;
  let fixture: ComponentFixture<EmployeeIncomeCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeIncomeCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeIncomeCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
