import { Component, OnInit, TemplateRef } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UtilityService } from "../../../services/utility.service";
import { CustomResponse } from "@app/shared/models/custom-response.model";
import { EmployeeIncomeCode } from "../../../models/employee-income-code.model";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { TextboxQuestion } from "../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-textbox";
import { DynamicFormService } from "@app/shared/components/dynamic-form/services/dynamic-form.service";

@Component({
  selector: "simpliflysaas-employee-income-code",
  templateUrl: "./employee-income-code.component.html",
  styleUrls: ["./employee-income-code.component.scss"]
})
export class EmployeeIncomeCodeComponent implements OnInit {
  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  // dynamic form
  employeeIncomeCodeForm: FormGroup;
  submitted: boolean;

  // data table
  employeeIncomeCodeLoading: boolean;
  employeeIncomeCode: any;
  dataPerPage: number = 5;
  processingAction: boolean;
  employeeIncomeCodeFormControls = [
    new TextboxQuestion({
      key: "description",
      label: "Description",
      required: true,
      order: 1
    }),
    new TextboxQuestion({
      key: "income_code",
      label: "Income Code",
      required: true,
      order: 2
    })
  ];

  constructor(
    private utilityService: UtilityService,
    private modalService: BsModalService,
    private toastrMessageService: ToastrMessageService,
    private dynamicFormService: DynamicFormService
  ) {}

  ngOnInit() {
    this.employeeIncomeCodeLoading = true;
    this.getEmployeeIncomeCodeList();
  }

  openModel(template: TemplateRef<any>): void {
    this.closeEditMode();
    this.modalRef = this.modalService.show(template, this.config);
    this.buildEmployeeIncomeCodeForm();
  }

  closeModal() {
    this.submitted = false;
    this.modalRef.hide();
  }

  buildEmployeeIncomeCodeForm(): void {
    this.employeeIncomeCodeForm = this.dynamicFormService.createFormGroup(
      this.employeeIncomeCodeFormControls
    );
  }

  addEmployeeIncomeCode(): void {
    this.submitted = true;

    if (this.employeeIncomeCodeForm.invalid) return;

    this.utilityService
      .addEmployeeIncomeCode(this.employeeIncomeCodeForm.value)
      .subscribe(
        response => {
          if (response && response.status) {
            this.toastrMessageService.showSuccess(response.data);
            return;
          }

          //preview server side validation messages
          this.toastrMessageService.showError(response.data);
        },
        error => {
          this.toastrMessageService.showError(error);
        },
        () => {
          this.closeModal();
          this.getEmployeeIncomeCodeList();
        }
      );
  }

  getEmployeeIncomeCodeList(): void {
    this.employeeIncomeCodeLoading = true;
    this.utilityService.getEmployeeIncomeCodeList().subscribe(
      (response: CustomResponse) => {
        if (response && response.status) {
          this.employeeIncomeCode = response.data;
          return;
        } else {
          this.employeeIncomeCode = null;
        }
      },
      error => {
        this.toastrMessageService.showError(error);

        this.employeeIncomeCodeLoading = false;
      },
      () => {
        // this.closeEditMode();

        this.employeeIncomeCodeLoading = false;
      }
    );
  }

  updateEmployeeIncomeCode(employeeIncomeCode): void {
    const id = employeeIncomeCode.original_data.id;
    delete employeeIncomeCode.original_data;

    this.processingAction = true;
    this.utilityService
      .updateEmployeeIncomeCode(id, employeeIncomeCode)
      .subscribe(
        response => {
          if (response && response.status) {
            this.toastrMessageService.showSuccess(response.data);
          }
        },
        error => {
          this.toastrMessageService.showError(error);
        },
        () => {
          this.getEmployeeIncomeCodeList();
          this.processingAction = false;
        }
      );
  }

  deleteEmployeeIncomeCode(employeeIncomeCode: EmployeeIncomeCode): void {
    this.processingAction = true;
    this.utilityService
      .deleteEmployeeIncomeCode(employeeIncomeCode.id)
      .subscribe(
        response => {
          if (response && response.status) {
            this.toastrMessageService.showSuccess(response.data);
          }
        },
        error => {
          this.toastrMessageService.showError(error);
        },
        () => {
          this.getEmployeeIncomeCodeList();
          this.processingAction = false;
        }
      );
  }

  /**
   * This method executes when the user clicks cancel during edit phase.
   */
  showSaveOption: boolean;
  closeEditMode(): void {
    this.showSaveOption = false;
  }

  openConfirmDialog(employeeIncomeCode: EmployeeIncomeCode): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.action = "delete";
    this.modalRef.content.data = employeeIncomeCode.income_code;
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deleteEmployeeIncomeCode(employeeIncomeCode);
      }
    });
  }
}
