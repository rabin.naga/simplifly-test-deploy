import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HelperService } from "@app/shared/services/helper/helper.service";
import { BreadcrumbsService } from "ng6-breadcrumbs";

@Component({
  selector: "simpliflysaas-sub-utility",
  templateUrl: "./sub-utility.component.html",
  styleUrls: ["./sub-utility.component.scss"]
})
export class SubUtilityComponent implements OnInit {
  subUtilityTitle: string;
  selectedUtility: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private helperService: HelperService,
    private breadcrumbsService: BreadcrumbsService
  ) {}

  ngOnInit() {
    this.checkSelectedUtility();

    this.breadcrumbsService.store([
      { label: "Dashboard", url: "/", params: [] },
      { label: "Utilities", url: "/utilities", params: [] },
      {
        label: this.subUtilityTitle,
        url: "/utitlites/{{this.selectedUtility}}",
        params: []
      }
    ]);
  }

  checkSelectedUtility(): void {
    this.selectedUtility = this.activatedRoute.snapshot.params["title"];
    this.subUtilityTitle = this.helperService.replaceHyphenWithSpace(
      this.selectedUtility
    );
  }
}
