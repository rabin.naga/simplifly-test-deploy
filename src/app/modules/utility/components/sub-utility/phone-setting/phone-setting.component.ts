import { Component, OnInit, TemplateRef } from "@angular/core";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap";
import { UtilityService } from "@app/modules/utility/services/utility.service";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { TextboxQuestion } from "../../../../../../../projects/simpliflysaas-data-table/src/lib/models/question-textbox";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { PhoneSetting } from "./../../../models/phone-setting.model";
import { RegexConst } from "@app/shared/constants/regex.constant";
@Component({
  selector: "simpliflysaas-phone-setting",
  templateUrl: "./phone-setting.component.html",
  styleUrls: ["./phone-setting.component.scss"]
})
export class PhoneSettingComponent implements OnInit {
  phoneSettingForm: FormGroup;
  phoneSettingLoading: boolean;
  submitted: boolean;
  phoneSetting: PhoneSetting[];
  regexConst = RegexConst;

  // modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  // table column
  dataPerPage: number = 5;
  processingAction: boolean;
  phoneSettingColumns = [
    new TextboxQuestion({
      key: "receiver_country_code",
      label: "Receiver Country Code",
      required: true,
      order: 1,
      pattern: this.regexConst.COUNTRY_CODE
    }),
    new TextboxQuestion({
      key: "sender_no",
      label: "Sender No",
      required: true,
      order: 2,
      pattern: this.regexConst.PHONE_NO
    })
  ];

  constructor(
    private utilityService: UtilityService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private toastrMessageService: ToastrMessageService
  ) {}

  ngOnInit() {
    this.phoneSettingLoading = true;
    this.getPhoneSetting();
  }
  openModel(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, this.config);
    this.closeEditMode();
    this.buildPhoneSettingForm();
  }

  closeModal(): void {
    // setting it to true again to trigger ngOnChanges for closeEditMode for the next edit item
    this.showSaveOption = true;
    this.submitted = false;
    this.modalRef.hide();
  }

  // .......method to build form.......
  buildPhoneSettingForm(): void {
    this.phoneSettingForm = this.formBuilder.group({
      sender_no: [
        "",
        [Validators.required, Validators.pattern(this.regexConst.PHONE_NO)]
      ],
      receiver_country_code: [
        "",
        [Validators.required, Validators.pattern(this.regexConst.COUNTRY_CODE)]
      ]
    });
  }

  /**
   * Method to get Pay Period
   */
  getPhoneSetting(): void {
    this.utilityService.getPhoneSetting().subscribe(
      (response: any) => {
        if (response && response.status) {
          this.phoneSetting = response.data;
        } else {
          this.phoneSetting = [];
        }
      },
      error => {
        console.log(error);
        this.phoneSettingLoading = false;
      },
      () => {
        this.phoneSettingLoading = false;
      }
    );
  }

  /**
   * Method to add Pay Period
   */
  addPhoneSetting(): void {
    this.submitted = true;

    if (this.phoneSettingForm.invalid) return;

    this.utilityService.addPhoneSetting(this.phoneSettingForm.value).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
          return;
        }

        //preview server side validation messages
        this.toastrMessageService.showError(response.data);
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.closeModal();
        this.getPhoneSetting();
      }
    );
  }

  /**
   * Method to update Phone setting
   */
  updatePhoneSetting(phoneSetting: PhoneSetting): void {
    const id = phoneSetting.original_data.id;
    delete phoneSetting.original_data;

    this.utilityService.updatePhoneSetting(id, phoneSetting).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getPhoneSetting();
        this.processingAction = false;
      }
    );
  }

  /**
   * Method to delete Phone Setting
   */
  deletePhoneSetting(phoneSetting: PhoneSetting) {
    this.processingAction = true;
    this.utilityService.deletePhoneSetting(phoneSetting.id).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      (): void => {
        this.closeEditMode();
        this.getPhoneSetting();
        this.processingAction = false;
      }
    );
  }

  /**
   * This method executes on click of delete Phone Setting
   * @param phoneSetting
   */
  openConfirmDialog(phoneSetting: PhoneSetting): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.action = "delete";
    this.modalRef.content.data = phoneSetting.sender_no;
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.deletePhoneSetting(phoneSetting);
      }
    });
  }

  /**
   * This method executes when the user clicks cancel during edit phase.
   */
  showSaveOption: boolean;
  closeEditMode(): void {
    this.showSaveOption = false;
  }

  // form control names
  get SenderPhoneNumberFormControl() {
    return this.phoneSettingForm.get("sender_no");
  }

  get receiverCountryCodeFormControl() {
    return this.phoneSettingForm.get("receiver_country_code");
  }
}
