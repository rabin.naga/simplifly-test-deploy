export class SmsPresetMessage {
  id: number;
  category: number;
  client_id: number;
  created_by: number;
  created_datetime: string;
  message: string;
  staffing_company_id: number;
  original_data: originalDataType;
}
export class originalDataType {
  id: string;
}
