export class PayGroup {
    id?: string;
    pay_group: string;
    staffing_company_id?: string;
}
