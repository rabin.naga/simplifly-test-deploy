export class PayMethod {
    id?: string;
    pay_method: string;
    staffing_company_id: string;
}
