export class PayType {
    id?: string;
    category: number;
    is_not_invoice: number;
    is_not_pay: number;
    is_show: number;
    job_order_option: number;
    multiplier: number;
    pay_type: string;
}
