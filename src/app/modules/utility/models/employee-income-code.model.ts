export class EmployeeIncomeCode {
    id: string;
    description: string;
    income_code: string;
}
