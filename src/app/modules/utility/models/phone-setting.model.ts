export class PhoneSetting {
  id: number;
  receiver_country_code: number;
  sender_no: string;
  staffing_company_id: number;
  original_data: original_data;
}
export class original_data {
  id: string;
}
