export class PayPeriod {
    id?: string;
    pay_period: string;
    staffing_company_id: string;
}
