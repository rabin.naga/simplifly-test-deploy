import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { UtilityRoutingModule } from "./utility-routing.module";
import { UtilityComponent } from "./components/utility.component";
import { SharedModule } from "@app/shared/shared.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { UtilityListComponent } from "./components/utility-list/utility-list.component";
import { SubUtilityComponent } from "./components/sub-utility/sub-utility.component";
import { SimpliflysaasDataTableModule } from "../../../../projects/simpliflysaas-data-table/src/public-api";
import { CommonSubUtilityComponent } from "./components/sub-utility/common-sub-utility/common-sub-utility.component";
import { PayPeriodComponent } from "./components/sub-utility/pay-period/pay-period.component";
import { EmployeeIncomeCodeComponent } from "./components/sub-utility/employee-income-code/employee-income-code.component";
import { PayMethodComponent } from "./components/sub-utility/pay-method/pay-method.component";
import { PayGroupComponent } from "./components/sub-utility/pay-group/pay-group.component";
import { PayTypeComponent } from "./components/sub-utility/pay-type/pay-type.component";
import { PhoneSettingComponent } from "./components/sub-utility/phone-setting/phone-setting.component";
import { SmsPresetMessageComponent } from "./components/sub-utility/sms-preset-message/sms-preset-message.component";
import { EmailCampaignComponent } from "./components/sub-utility/email-campaign/email-campaign.component";
import { VisitComponent } from './components/sub-utility/visit/visit.component';
@NgModule({
  declarations: [
    UtilityComponent,
    SubUtilityComponent,
    UtilityListComponent,
    EmployeeIncomeCodeComponent,
    PayPeriodComponent,
    CommonSubUtilityComponent,
    PayMethodComponent,
    PayGroupComponent,
    PayTypeComponent,
    PhoneSettingComponent,
    SmsPresetMessageComponent,
    EmailCampaignComponent,
    VisitComponent
  ],
  imports: [
    CommonModule,
    UtilityRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    SimpliflysaasDataTableModule
  ]
})
export class UtilityModule {}
