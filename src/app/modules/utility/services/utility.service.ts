import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "@env/environment";
import { EmployeeIncomeCode } from "../models/employee-income-code.model";
import { PayPeriod } from "../models/pay-period.model";
import { HttpClientService } from "@app/core/services/http-client/http-client.service";

@Injectable({
  providedIn: "root"
})
export class UtilityService {
  public apiUrl = `${environment.baseIP}${environment.apiPrefix}`;

  constructor(private readonly httpClientService: HttpClientService) {}

  // ---------------------------Utility-----------------------------
  public addUtility(body: {}): Observable<any> {
    return this.httpClientService.post(`${this.apiUrl}utilities`, body);
  }

  public getUtilityList(): Observable<any> {
    return this.httpClientService.get(`${this.apiUrl}utilities`);
  }

  //------------------------- Common Sub Utility-----------------------
  public getSelectedUtilityList(utility): Observable<any> {
    return this.httpClientService.get(`${this.apiUrl}${utility}`);
  }

  public updateSelectedUtilityItem(utilityType, id, body): Observable<any> {
    return this.httpClientService.put(
      `${this.apiUrl}${utilityType}/${id}`,
      body
    );
  }

  public deleteSelectedUtilityItem(utilityType, id): Observable<any> {
    return this.httpClientService.delete(`${this.apiUrl}${utilityType}/${id}`);
  }

  public addSubUtilityItem(utilityType, body): Observable<any> {
    return this.httpClientService.post(`${this.apiUrl}${utilityType}`, body);
  }

  // ----------------------Employee Income Code------------------------------
  public getEmployeeIncomeCodeList(): Observable<any> {
    return this.httpClientService.get(`${this.apiUrl}employee-income-code`);
  }

  public addEmployeeIncomeCode(body: EmployeeIncomeCode): Observable<any> {
    return this.httpClientService.post(
      `${this.apiUrl}employee-income-code`,
      body
    );
  }

  public deleteEmployeeIncomeCode(id: string): Observable<any> {
    return this.httpClientService.delete(
      `${this.apiUrl}employee-income-code/${id}`
    );
  }

  public updateEmployeeIncomeCode(
    id,
    employeeIncomeCode: EmployeeIncomeCode
  ): Observable<any> {
    return this.httpClientService.put(
      `${this.apiUrl}employee-income-code/${id}`,
      employeeIncomeCode
    );
  }

  // ------------------------Pay Period-----------------------------------
  public getPayPeriod(): Observable<any> {
    return this.httpClientService.get(`${this.apiUrl}pay-period`);
  }

  public addPayPeriod(body: PayPeriod): Observable<any> {
    return this.httpClientService.post(`${this.apiUrl}pay-period`, body);
  }

  public updatePayPeriod(id, payPeriod): Observable<any> {
    return this.httpClientService.put(
      `${this.apiUrl}pay-period/${id}`,
      payPeriod
    );
  }

  public deletePayPeriod(id): Observable<any> {
    return this.httpClientService.delete(`${this.apiUrl}pay-period/${id}`);
  }

  // ------------------------Pay Method-------------------------------------
  public getPayMethod(): Observable<any> {
    return this.httpClientService.get(`${this.apiUrl}pay-method`);
  }

  public updatePayMethod(id, payMethod): Observable<any> {
    return this.httpClientService.put(
      `${this.apiUrl}pay-method/${id}`,
      payMethod
    );
  }

  public addPayMethod(payMethod): Observable<any> {
    return this.httpClientService.post(`${this.apiUrl}pay-method`, payMethod);
  }

  public deletePayMethod(id): Observable<any> {
    return this.httpClientService.delete(`${this.apiUrl}pay-method/${id}`);
  }
  // .............................visit.....................................

  public getVisitType(): Observable<any> {
    return this.httpClientService.get(`${this.apiUrl}visit`);
  }

  public updateVisitType(id, visitType): Observable<any> {
    return this.httpClientService.put(`${this.apiUrl}visit/${id}`, visitType);
  }

  public addVisitType(visitType): Observable<any> {
    return this.httpClientService.post(`${this.apiUrl}visit`, visitType);
  }

  public deleteVisitType(id): Observable<any> {
    return this.httpClientService.delete(`${this.apiUrl}visit/${id}`);
  }

  // ----------------------------Pay Group------------------------------------
  public getPayGroup(): Observable<any> {
    return this.httpClientService.get(`${this.apiUrl}pay-group`);
  }

  public updatePayGroup(id, payGroup): Observable<any> {
    return this.httpClientService.put(
      `${this.apiUrl}pay-group/${id}`,
      payGroup
    );
  }

  public addPayGroup(payGroup): Observable<any> {
    return this.httpClientService.post(`${this.apiUrl}pay-group`, payGroup);
  }

  public deletePayGroup(id): Observable<any> {
    return this.httpClientService.delete(`${this.apiUrl}pay-group/${id}`);
  }

  // -------------------------Pay Type----------------------------------
  public getPayType(): Observable<any> {
    return this.httpClientService.get(`${this.apiUrl}pay-type`);
  }

  public updatePayType(id, payType): Observable<any> {
    return this.httpClientService.put(`${this.apiUrl}pay-type/${id}`, payType);
  }

  public addPayType(payType): Observable<any> {
    return this.httpClientService.post(`${this.apiUrl}pay-type`, payType);
  }

  public deletePayType(id): Observable<any> {
    return this.httpClientService.delete(`${this.apiUrl}pay-type/${id}`);
  }

  // -------------------Phone Setting----------------------------
  public getPhoneSetting(): Observable<any> {
    return this.httpClientService.get(`${this.apiUrl}phone-setting`);
  }
  public updatePhoneSetting(id, phoneSetting): Observable<any> {
    return this.httpClientService.put(
      `${this.apiUrl}phone-setting/${id}`,
      phoneSetting
    );
  }
  public addPhoneSetting(phoneSetting): Observable<any> {
    return this.httpClientService.post(
      `${this.apiUrl}phone-setting`,
      phoneSetting
    );
  }
  public deletePhoneSetting(id): Observable<any> {
    return this.httpClientService.delete(`${this.apiUrl}phone-setting/${id}`);
  }

  // -------------------------SMS Preset Message------------------------------
  public getSmsPresetMessage(): Observable<any> {
    return this.httpClientService.get(`${this.apiUrl}sms-preset-message`);
  }

  public updateSmsPresetMessage(id, smsPresetMessage): Observable<any> {
    return this.httpClientService.put(
      `${this.apiUrl}sms-preset-message/${id}`,
      smsPresetMessage
    );
  }

  public addSmsPresetMessage(smsPresetMessage): Observable<any> {
    return this.httpClientService.post(
      `${this.apiUrl}sms-preset-message`,
      smsPresetMessage
    );
  }

  public deleteSmsPresetMessage(id): Observable<any> {
    return this.httpClientService.delete(
      `${this.apiUrl}sms-preset-message/${id}`
    );
  }

  // ------------------------Email Campaign------------------------------
  public getEmailCampaign(): Observable<any> {
    return this.httpClientService.get(`${this.apiUrl}email-campaign`);
  }

  public updateEmailCampaign(id, emailCampaign): Observable<any> {
    return this.httpClientService.put(
      `${this.apiUrl}email-campaign/${id}`,
      emailCampaign
    );
  }

  public addEmailCampaign(emailCampaign): Observable<any> {
    return this.httpClientService.post(
      `${this.apiUrl}email-campaign`,
      emailCampaign
    );
  }

  public deleteEmailCampaign(id): Observable<any> {
    return this.httpClientService.delete(`${this.apiUrl}email-campaign/${id}`);
  }
}
