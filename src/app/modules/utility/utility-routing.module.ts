import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UtilityComponent } from "./components/utility.component";
import { SubUtilityComponent } from "./components/sub-utility/sub-utility.component";

const routes: Routes = [
  { path: "", component: UtilityComponent },
  {
    path: ":title",
    component: SubUtilityComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UtilityRoutingModule {}
