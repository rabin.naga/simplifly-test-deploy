import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ClassCodeRoutingModule } from "./class-code-routing.module";
import { ClassCodeComponent } from "./class-code/class-code.component";
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "@app/shared/shared.module";

@NgModule({
  declarations: [ClassCodeComponent],
  imports: [
    CommonModule,
    SharedModule,
    ClassCodeRoutingModule,
    TranslateModule.forChild(),
    ClassCodeRoutingModule,
    TranslateModule.forChild(),
    SharedModule
  ]
})
export class ClassCodeModule {}
