import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-class-code",
  templateUrl: "./class-code.component.html",
  styleUrls: ["./class-code.component.scss"]
})
export class ClassCodeComponent implements OnInit {
  constructor(private translate: TranslateService) {}

  ngOnInit() {
    this.translate.use("en");
    this.translate.setTranslation("en", Nepali);
  }
}

export const English = {
  HOME: {
    TITLE: "Hello in English!"
  }
};

export const Nepali = {
  HOME: {
    TITLE: "Namaste in Nepali!"
  }
};
