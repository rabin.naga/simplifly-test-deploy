import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassCodeComponent } from './class-code/class-code.component';

const routes: Routes = [
  {path: '', component: ClassCodeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassCodeRoutingModule { }
