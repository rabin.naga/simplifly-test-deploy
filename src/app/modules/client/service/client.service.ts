import { Client } from "./../models/client.model";
import { Injectable } from "@angular/core";
import { environment } from "@env/environment";
import { Observable } from "rxjs";
import { ApiRouteConst } from "@app/shared/constants/api-route.constant";
import { HttpClientService } from "@app/core/services/http-client/http-client.service";
import { Iclient } from "../models/Iclient";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ClientService {
  private _url = "http://localhost:3000/client";
  // private _url = "http://192.168.1.39:3000/client";

  baseIp = environment.baseIP;
  apiPrefix = environment.apiPrefix;
  selectedClient: any;

  constructor(
    private httpClientService: HttpClientService,
    // this is for json server
    private http: HttpClient
  ) {}

  getClientList(): Observable<Iclient[]> {
    return this.http.get<Iclient[]>(this._url);
  }

  getClientByClientId(clientId): Observable<Iclient> {
    return this.http.get<Iclient>(`${this._url}/${clientId}`);
  }

  addClient(body): Observable<Iclient> {
    return this.http.post<Iclient>(this._url, body, {
      headers: new HttpHeaders({
        "Content-type": "application/json"
      })
    });
  }

  setSelectedClient(client): void {
    this.selectedClient = client;
  }

  getSelectedClient() {
    return this.selectedClient;
  }

  editClientInformation(clientId, requestBody): Observable<void> {
    return this.http.put<void>(`${this._url}/${clientId}`, requestBody, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    });
  }

  // getClientDetailByClientId(id: number): Observable<any> {
  //   return this.http.get(`${this._url}/${id}`);
  // }
}

// this service is made for dummy api...using json server..
