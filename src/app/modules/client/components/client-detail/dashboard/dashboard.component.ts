import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { GoogleMapLocation } from "./../../../models/map-location";
import { GeocodeService } from "./../../../../../shared/services/geocode/geocode.service";
import { ClientService } from "./../../../service/client.service";
import { Iclient } from "./../../../models/Iclient";

@Component({
  selector: "simpliflysaas-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  clientDetail: Iclient;

  location: GoogleMapLocation;
  loading: boolean;
  address: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private clientService: ClientService,
    private geoCodeService: GeocodeService,
    private _route: Router,

    private ref: ChangeDetectorRef
  ) {}

  ngOnInit() {
    const client_id = this.activatedRoute.snapshot.params.id;
    this.getClientDetailById(client_id);
  }

  /**
   * Call API to get client details by id
   * @param id
   */
  getClientDetailById(id: number): void {
    this.clientService.getClientByClientId(id).subscribe(
      response => {
        this.clientDetail = response;

        this.addressToCoordinates();
      },
      error => {
        console.log(error);
      }
    );
  }
  // edit client detail...
  editClientInformation(id: string) {
    this.clientService.setSelectedClient(this.clientDetail);
    this._route.navigate(["/client/edit", id]);
  }

  // google api to convert address:string into lat and lng

  addressToCoordinates(): void {
    if (this.clientDetail) {
      this.loading = true;
      this.geoCodeService
        .geocodeAddress(this.clientDetail.address1)
        .subscribe((location: GoogleMapLocation) => {
          this.location = location;
          this.loading = false;
          this.ref.detectChanges();
        });
    }
  }
}
