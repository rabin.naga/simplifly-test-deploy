import { ValidationMessageService } from "./../../../../../shared/services/validation-message/validation-message.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "simpliflysaas-pricing",
  templateUrl: "./pricing.component.html",
  styleUrls: ["./pricing.component.scss"]
})
export class PricingComponent implements OnInit {
  pricingForm: FormGroup;
  submitted: Boolean;
  language: string;
  constructor(
    private fb: FormBuilder,
    private validationMessageService: ValidationMessageService
  ) {}
  validationMessages = {
    enUS: {
      name: {
        required: " Name is required"
      },

      state: {
        required: "State is required"
      },
      classification: {
        required: "Classification is required"
      },
      workersCompensation: {
        required: "Workers'Compensation is required"
      }
    }
  };
  // object for saving error message when validation fails
  formErrors = {
    name: "",
    state: "",
    classification: "",
    workersCompensation: ""
  };

  ngOnInit() {
    this.buildPricingForm();
    this.pricingForm.valueChanges.subscribe(data => {
      this.logValidationErrors();
    });
  }
  buildPricingForm(): void {
    this.pricingForm = this.fb.group({
      name: ["", [Validators.required]],
      state: ["", [Validators.required]],
      classification: ["", [Validators.required]],
      workersCompensation: ["", [Validators.required]],
      markup: [""],
      clientBillRate: [""]
    });
  }

  /**
   * adds validation message to the form control on violation
   */
  logValidationErrors(): void {
    if (!this.submitted) return;
    this.formErrors = this.validationMessageService.getFormErrors(
      this.pricingForm,
      this.formErrors,
      this.validationMessages,
      this.language
    );
  }

  onSubmit() {
    this.submitted = true;
    this.logValidationErrors();
  }
}
