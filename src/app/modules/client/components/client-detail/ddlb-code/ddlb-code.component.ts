import { ValidationMessageService } from "./../../../../../shared/services/validation-message/validation-message.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "simpliflysaas-ddlb-code",
  templateUrl: "./ddlb-code.component.html",
  styleUrls: ["./ddlb-code.component.scss"]
})
export class DdlbCodeComponent implements OnInit {
  ddlbCodeForm: FormGroup;
  submitted: Boolean;
  language: string;
  constructor(
    private fb: FormBuilder,
    private validationMessageService: ValidationMessageService
  ) {}

  validationMessages = {
    enUS: {
      departmentName: {
        required: "Department Name is required"
      },

      title: {
        required: "Title is required"
      },
      locationName: {
        required: "Location Name is required"
      },
      state: {
        required: "State is required"
      },
      zip: {
        required: "Zip is required"
      }
    }
  };
  // object for saving error message when validation fails
  formErrors = {
    departmentName: "",
    title: "",
    locationName: "",
    state: "",
    zip: ""
  };

  ngOnInit() {
    this.buildDDLBcodeForm();
    this.ddlbCodeForm.valueChanges.subscribe(data => {
      this.logValidationErrors();
    });
  }
  buildDDLBcodeForm() {
    this.ddlbCodeForm = this.fb.group({
      departmentName: ["", [Validators.required]],
      supervisorName: [""],
      telephone: [""],
      title: ["", [Validators.required]],
      locationName: ["", [Validators.required]],
      locationAddress: [""],
      city: [""],
      state: ["", [Validators.required]],
      zip: ["", [Validators.required]],
      custcode: [""],
      lateCharge: [""],
      paymentType: [""],
      paymentDays: [""]
    });
  }

  /**
   * adds validation message to the form control on violation
   */
  logValidationErrors(): void {
    if (!this.submitted) return;
    this.formErrors = this.validationMessageService.getFormErrors(
      this.ddlbCodeForm,
      this.formErrors,
      this.validationMessages,
      this.language
    );
  }

  onSubmit() {
    this.submitted = true;
    this.logValidationErrors();
  }
}
