import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DdlbCodeComponent } from './ddlb-code.component';

describe('DdlbCodeComponent', () => {
  let component: DdlbCodeComponent;
  let fixture: ComponentFixture<DdlbCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DdlbCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DdlbCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
