import { Component, OnInit, ViewChild } from "@angular/core";
import { TabsetComponent, TabDirective } from "ngx-bootstrap/tabs";
@Component({
  selector: "simpliflysaas-client-detail",
  templateUrl: "./client-detail.component.html",
  styleUrls: ["./client-detail.component.scss"]
})
export class ClientDetailComponent implements OnInit {
  @ViewChild("tabset") tabset: TabsetComponent;
  constructor() {}
  ngOnInit() {}
}
