import { Iclient } from "./../../models/Iclient";
import { Component, OnInit } from "@angular/core";

import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ClientService } from "./../../service/client.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { HelperService } from "@app/shared/services/helper/helper.service";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { Client } from "./../../models/client.model";
import { CustomResponse } from "@app/shared/models/custom-response.model";

@Component({
  selector: "simpliflysaas-client-list",
  templateUrl: "./client-list.component.html",
  styleUrls: ["./client-list.component.scss"]
})
export class ClientListComponent implements OnInit {
  clientList: Iclient[];
  clientCount: number;
  clientToBeDeleted: Client;
  clientListLoading: boolean;
  noResult: boolean;

  constructor(
    private clientService: ClientService,
    private toastr: ToastrService,
    private router: Router,
    private modalService: BsModalService,
    private helperService: HelperService
  ) {}

  ngOnInit() {
    this.getClient();
  }

  /**
   * Call get admin API
   */
  getClient(): void {
    this.clientListLoading = true;
    this.clientService.getClientList().subscribe(
      response => {
        this.clientList = response;
        if (JSON.stringify(response.length) == "0") {
          this.noResult = true;
        }
      },
      error => {
        this.clientListLoading = false;
      },
      () => {
        this.clientListLoading = false;
      }
    );
  }

  navigateToClientDetail(clientId) {
    this.router.navigate(["client", clientId]);
  }
}
