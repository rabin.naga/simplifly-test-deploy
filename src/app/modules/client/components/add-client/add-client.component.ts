import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { Iclient } from "./../../models/Iclient";
import { PayTypeFormArray } from "./../../models/Iclient";
import { ContactFormArray } from "./../../models/Iclient";
import { DatePipe, Location } from "@angular/common";

import { checkPassword } from "@app/shared/directives/validators/check-password";
import { RegexConst } from "@app/shared/constants/regex.constant";
import { ClientService } from "./../../service/client.service";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ValidationMessageService } from "@app/shared/services/validation-message/validation-message.service";

@Component({
  selector: "simpliflysaas-add-client",
  templateUrl: "./add-client.component.html",
  styleUrls: ["./add-client.component.scss"],
  providers: [DatePipe]
})
export class AddClientComponent implements OnInit {
  clientForm: FormGroup;
  selectedClient: Iclient;
  client: Iclient;
  uploading: boolean;
  regexConst = RegexConst;
  submitted: boolean;
  uploadingClient: boolean;
  language: string;
  submitButton: string;
  editMode: boolean;
  payTypeSubmitted: boolean;
  contactSubmitted: boolean;
  date: Date;

  validationMessages = {
    enUS: {
      company_name: {
        required: "Email cannot be blank"
      },
      email: {
        pattern: "Email is invalid"
      },
      telephone: {
        pattern: "Telephone is invalid"
      },
      state: {
        required: "State cannot be blank"
      },
      username: {
        required: "Username cannot be blank"
      },
      password: {
        required: "Password cannot be blank"
      }
    }
  };
  // object for saving error message when validation fails
  formErrors = {
    company_name: "",
    email: "",
    telephone: "",
    state: "",
    username: "",
    password: ""
  };
  constructor(
    private fb: FormBuilder,
    private clientService: ClientService,
    private toastrMessageService: ToastrMessageService,
    private router: Router,
    private validationMessageService: ValidationMessageService,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe,
    private location: Location
  ) {}

  ngOnInit() {
    this.buildClientForm();
    this.clientForm.valueChanges.subscribe(data => {
      this.logValidationErrors();
    });
    this.analyzeRoute();
  }

  /**
   * render add admin or edit admin according to the route
   */
  analyzeRoute(): void {
    if (this.router.url.includes("edit")) {
      this.submitButton = "Edit";
      this.editMode = true;

      this.selectedClient = this.clientService.getSelectedClient();

      this.getSelectedClient();
    } else {
      this.submitButton = "Save";
    }
  }

  // this method initializes Client form
  buildClientForm(): void {
    const client = this.selectedClient;
    this.clientForm = this.fb.group(
      {
        company_name: [client ? client.company_name : "", Validators.required],
        email: [
          client ? client.email : "",
          [Validators.pattern(this.regexConst.EMAIL)]
        ],
        date: [client ? client.date : ""],
        telephone: [
          client ? client.telephone : "",
          [Validators.pattern(this.regexConst.PHONE_NO)]
        ],
        ext: [client ? client.ext : ""],
        address1: [client ? client.address1 : ""],
        address2: [client ? client.address2 : ""],
        fax: [client ? client.fax : ""],
        city: [client ? client.city : ""],
        state: [client ? client.state : "", [Validators.required]],
        zip: [client ? client.zip : ""],
        branch: [client ? client.branch : ""],
        location: [client ? client.location : ""],
        headquater: [client ? client.headquater : ""],
        username: [client ? client.username : "", [Validators.required]],
        password: ["", Validators.required],
        status: [client ? client.status : ""],
        logo: [client ? client.logo : ""],
        payTypeFormArray: this.fb.array([this.addPayTypeFormGroup()]),

        contactFormArray: this.fb.array([this.addContactFormGroup()])
      },
      { Validator: checkPassword }
    );
  }

  // this block of code is used to show form array data in the template.....
  setPayTypeFormArrayData(paytype: PayTypeFormArray[]) {
    const payTypeFormArray = new FormArray([]);
    paytype.forEach(element => {
      payTypeFormArray.push(
        this.fb.group({
          payType: element.payType,
          multiplier: element.multiplier
        })
      );
    });
    return payTypeFormArray;
  }
  setPayTypeFormArraycontrol() {
    this.clientForm.setControl(
      "payTypeFormArray",
      this.setPayTypeFormArrayData(this.selectedClient.payTypeFormArray)
    );
  }

  setContactFormArrayData(contact: ContactFormArray[]) {
    const contactFormArray = new FormArray([]);
    contact.forEach(element => {
      contactFormArray.push(
        this.fb.group({
          title: element.title,
          name: element.name,
          contactEmail: element.contactEmail,
          contactExt: element.contactExt,
          cellPhone: element.cellPhone,
          emailReport: element.emailReport
        })
      );
    });
    return contactFormArray;
  }

  setContactFormArraycontrol() {
    this.clientForm.setControl(
      "contactFormArray",
      this.setContactFormArrayData(this.selectedClient.contactFormArray)
    );
  }
  // .......................................................

  /**
   * adds validation message to the form control on violation
   */
  logValidationErrors(): void {
    if (!this.submitted) return;
    this.formErrors = this.validationMessageService.getFormErrors(
      this.clientForm,
      this.formErrors,
      this.validationMessages,
      this.language
    );
  }

  /**
   * get the selected client data
   */
  getSelectedClient(): void {
    const clientId = this.activatedRoute.snapshot.params["id"];

    this.clientService.getClientByClientId(clientId).subscribe(
      response => {
        this.selectedClient = response;
        this.buildClientForm();

        // calling method to show all payTypeformArray Data...
        this.setPayTypeFormArraycontrol();
        // calling method to show contactformArray Data...
        this.setContactFormArraycontrol();
        return;
      },

      error => {
        console.log(error);
      }
    );
  }

  /**
   * call update admin API for editing admin information
   */
  editClientInformation(): void {
    this.submitted = true;
    if (
      this.clientForm.controls["password"] &&
      this.clientForm.controls["password"].errors
    ) {
      this.clientForm.controls["password"].setErrors(null);
    }

    if (this.clientForm.valid && this.clientForm.pristine) {
      this.router.navigate(["/client"]);
      return;
    }

    this.clientForm.removeControl("headquater");
    this.clientForm.removeControl("password");
    this.clientForm.removeControl("username");

    this.uploadingClient = true;
    if (this.clientForm.invalid) return;

    const clientId = this.selectedClient.id;

    this.clientService
      .editClientInformation(clientId, this.clientForm.value)
      .subscribe(
        response => {
          // this.toastrMessageService.showSuccess("Client edited successfully");
          this.router.navigate(["/client"]);
          return;

          // this.toastrMessageService.showError(response.data);
        },
        error => {
          this.toastrMessageService.showError(error);
        },
        () => {
          this.uploadingClient = false;
          1;
        }
      );
  }

  addPayTypeFormGroup(): FormGroup {
    const client: Iclient = this.selectedClient;

    return this.fb.group({
      payType: [
        client ? client.payTypeFormArray[0].payType : "",
        Validators.required
      ],
      multiplier: [
        client ? client.payTypeFormArray[0].multiplier : "",
        Validators.required
      ]
    });
  }

  addNewPayTypeFormGroup(): FormGroup {
    return this.fb.group({
      payType: ["", Validators.required],
      multiplier: ["", Validators.required]
    });
  }

  addContactFormGroup(): FormGroup {
    const client = this.selectedClient;

    return this.fb.group({
      title: [
        client ? client.contactFormArray[0].title : "",
        Validators.required
      ],
      name: [
        client ? client.contactFormArray[0].name : "",
        Validators.required
      ],
      contactEmail: [
        client ? client.contactFormArray[0].contactEmail : "",
        [Validators.required, Validators.pattern(this.regexConst.EMAIL)]
      ],
      contactExt: [
        client ? client.contactFormArray[0].contactExt : "",
        Validators.required
      ],
      cellPhone: [
        client ? client.contactFormArray[0].cellPhone : "",
        [Validators.required, Validators.pattern(this.regexConst.PHONE_NO)]
      ],
      emailReport: [client ? client.contactFormArray[0].emailReport : ""]
    });
  }
  addNewContactFormGroup(): FormGroup {
    return this.fb.group({
      title: ["", Validators.required],
      name: ["", Validators.required],
      contactEmail: [
        "",
        [Validators.required, Validators.pattern(this.regexConst.EMAIL)]
      ],
      contactExt: ["", [Validators.required]],
      cellPhone: [
        "",
        [Validators.required, Validators.pattern(this.regexConst.PHONE_NO)]
      ],
      emailReport: [""]
    });
  }

  addPayTypeRow() {
    this.submitted = true;

    if (this.clientForm.get("payTypeFormArray").invalid) return;

    (<FormArray>this.clientForm.get("payTypeFormArray")).push(
      this.addNewPayTypeFormGroup()
    );
    this.submitted = false;
  }
  deletePayTypeRow(i: number) {
    (<FormArray>this.clientForm.get("payTypeFormArray")).removeAt(i);
  }

  addContactRow() {
    this.date = this.clientForm.get("date").value;

    this.submitted = true;
    if (this.clientForm.get("contactFormArray").invalid) return;

    (<FormArray>this.clientForm.get("contactFormArray")).push(
      this.addNewContactFormGroup()
    );
    this.submitted = false;
  }

  deleteContactRow(i: number) {
    (<FormArray>this.clientForm.get("contactFormArray")).removeAt(i);
  }
  transFormDateFormat() {
    const body = this.clientForm.value;
    body.date = this.datePipe.transform(body.date, "MM-dd-yyyy");
    this.clientForm.get("date").setValue(body.date);
  }
  addClient() {
    const clientId = this.activatedRoute.snapshot.params["id"];
    this.submitted = true;
    this.logValidationErrors();
    this.transFormDateFormat();

    this.uploadingClient = true;
    if (clientId) {
      this.editClientInformation();

      // console.log(this.clientForm.value);
    } else {
      if (this.clientForm.invalid) return;
      this.clientService.addClient(this.clientForm.value).subscribe(
        () => {
          this.router.navigate(["client"]);
        },

        error => {
          this.toastrMessageService.showError(error);
        },
        () => {
          this.uploadingClient = false;
        }
      );
    }
  }

  url: any = "";
  /**
   * called each time file input changes
   * @param event - image data
   */
  onSelectFile(event): void {
    // file is not selected i.e user clicks on cancel while uploading
    if (event.target.files.length === 0) {
      // remove validation error messages
      if (this.clientForm.controls["logo"].hasError("invalidImageSize")) {
        delete this.clientForm.controls["logo"].errors["invalidImageSize"];
      }

      if (this.clientForm.controls["logo"].hasError("invalidImageType")) {
        delete this.clientForm.controls["logo"].errors["invalidImageType"];
      }
      // remove image preview
      this.url = "";
      this.clientForm.controls["logo"].updateValueAndValidity();
    }

    // file is selected
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      // verify whether the uploaded file is image or not
      if (!event.target.files[0].type.match("image/", /.(jpg|jpeg|png)$/i)) {
        // this.uploading = false;
        this.url = "";
        this.clientForm.controls["logo"].setErrors({ invalidImageType: true });
        return;
      }

      // verify whether the file is more than 2MB
      if (event.target.files[0].size > 2097152) {
        // 2097152 is equal to 2 MB
        this.url = event.target.result;
        // this.uploading = false;
        this.clientForm.controls["logo"].setErrors({ invalidImageSize: true });
        reader.onload = (event: any) => {
          this.url = event.target.result;
        };
        this.clientForm.updateValueAndValidity();
        return;
      }

      this.uploading = true;
      reader.onload = (event: any) => {
        // called once readAsDataURL is completed
        this.url = event.target.result;
        this.uploading = false;
        this.clientForm.patchValue({ logo: event.target.result });
        this.clientForm.markAsDirty();
      };
    }
  }

  get payTypeFormArrayData() {
    return <FormArray>this.clientForm.get("payTypeFormArray");
  }
  get contactFormArrayData() {
    return <FormArray>this.clientForm.get("contactFormArray");
  }
  get logo() {
    return this.clientForm.get("logo");
  }

  onCancel(): void {
    this.location.back();
  }
}
