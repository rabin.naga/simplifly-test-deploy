import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AddClientComponent } from "./components/add-client/add-client.component";
import { ClientComponent } from "./components/client.component";
import { DashboardComponent } from "./components/client-detail/dashboard/dashboard.component";
import { ClientDetailComponent } from "./components/client-detail/client-detail.component";

const routes: Routes = [
  { path: "", component: ClientComponent },
  {
    path: "add",
    component: AddClientComponent,
    data: {
      breadcrumb: "Add Client"
    }
  },
  {
    path: "edit/:id",
    component: AddClientComponent,
    data: {
      breadcrumb: "Edit Client"
    }
  },
  {
    path: ":id",
    component: ClientDetailComponent,
    data: {
      breadcrumb: "Client Detail"
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule {}
