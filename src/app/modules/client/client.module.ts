import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ClientComponent } from "./components/client.component";
import { ClientRoutingModule } from "./client-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared/shared.module";
import { HttpClient } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { AddClientComponent } from "./components/add-client/add-client.component";
import { ClientListComponent } from "./components/client-list/client-list.component";
import { ClientDetailComponent } from "./components/client-detail/client-detail.component";
import { DashboardComponent } from "./components/client-detail/dashboard/dashboard.component";
import { PricingComponent } from "./components/client-detail/pricing/pricing.component";
import { DdlbCodeComponent } from "./components/client-detail/ddlb-code/ddlb-code.component";
// // ....for google map....
import { AgmCoreModule } from "@agm/core";
import { GeocodeService } from "./../../shared/services/geocode/geocode.service";
// // .....................
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/admin/", ".json");
}

@NgModule({
  declarations: [
    ClientComponent,
    AddClientComponent,
    ClientListComponent,
    ClientDetailComponent,
    DashboardComponent,
    PricingComponent,
    DdlbCodeComponent
    // GoogleMapsAPIWrapper
  ],
  providers: [GeocodeService],
  imports: [
    CommonModule,
    ClientRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      },
      isolate: true
    }),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyCbtx6HD4ZY5BCZUncqj5l86YaojtQPxNw"
    })
  ]
})
export class ClientModule {}
