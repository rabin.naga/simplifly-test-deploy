export interface Iclient {
  id: string;
  client_id: string;
  company_name: string;
  email: string;
  date: string;
  telephone: string;
  ext: string;
  address1: string;
  address2: string;
  fax: string;
  city: string;
  state: string;
  zip: string;
  branch: string;
  location: string;
  headquater: string;
  username: string;
  password: string;
  payType: string;
  status: string;
  logo: String;
  payTypeFormArray: PayTypeFormArray[];
  contactFormArray: ContactFormArray[];
}
export interface PayTypeFormArray {
  payType: string;
  multiplier: string;
}

export interface ContactFormArray {
  title: string;
  name: string;
  contactEmail: string;
  contactExt: string;
  cellPhone: string;
  emailReport: string;
}
