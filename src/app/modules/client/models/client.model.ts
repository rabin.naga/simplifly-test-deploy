export class Client {
  id: string;

  company_name: string;
  client_id: string;
  email: string;
  date: string;
  telephone: string;
  ext: string;
  address1: string;
  address2: string;
  fax: string;
  city: string;
  state: string;
  zip: string;
  branch: string;
  location: string;
  headquater: string;
  username: string;
  password: string;
  payType: string;
  multiplier: string;
  title: string;
  name: string;
  contactEmail: string;
  contactExt: string;
  cellPhone: string;
  emailReport: string;
}
