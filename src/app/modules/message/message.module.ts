import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MessageRoutingModule } from "./message-routing.module";
import { MessageComponent } from "./components/message.component";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared/shared.module";

import { HttpClient } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { MessageMenuComponent } from './components/message-menu/message-menu.component';
import { MessageListComponent } from './components/message-list/message-list.component';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/admin/", ".json");
}
@NgModule({
  declarations: [MessageComponent, MessageMenuComponent, MessageListComponent],
  imports: [
    CommonModule,
    MessageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      },
      isolate: true
    })
  ]
})
export class MessageModule {}
