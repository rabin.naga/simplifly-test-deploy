import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { SharedModule } from "@app/shared/shared.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MessagesComponent } from "./components/messages/messages.component";
import { TaskDragDropComponent } from "./components/task-drag-drop/task-drag-drop.component";
import { DashboardComponent } from "./components/dashboard.component";
import { TodoListComponent } from "./components/todo-list/todo-list.component";
import { ArchiveComponent } from "./components/todo-list/archive/archive.component";

@NgModule({
  declarations: [
    DashboardComponent,
    TodoListComponent,
    ArchiveComponent,
    MessagesComponent,
    TaskDragDropComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    DragDropModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DashboardModule {}
