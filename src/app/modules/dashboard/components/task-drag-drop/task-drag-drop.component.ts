import { Component, OnInit } from "@angular/core";
import { TrackService } from "../../services/track/track.service";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from "@angular/cdk/drag-drop";
import { Track } from "../../models/track.model";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";

@Component({
  selector: "simpliflysaas-task-drag-drop",
  templateUrl: "./task-drag-drop.component.html",
  styleUrls: ["./task-drag-drop.component.scss"]
})
export class TaskDragDropComponent implements OnInit {
  tracks: Track[];
  constructor(
    private trackService: TrackService,
    private toastrMessageService: ToastrMessageService
  ) {}

  ngOnInit() {
    this.getTracks();
  }

  /**
   * Calls get tracks API
   */
  getTracks(): void {
    this.trackService.getTracks().subscribe(
      response => {
        if (response && response.status) {
          this.tracks = response.data;
        }
      },
      error => {
        console.log(error);
        this.toastrMessageService.showError(error);
      }
    );
  }

  /**
   * An array of all track ids. Each id is associated with a `cdkDropList` for the
   * track talks. This property can be used to connect all drop lists together.
   */
  get trackIds(): string[] {
    return this.tracks.map(track => track.id);
  }

  onTalkDrop(event: CdkDragDrop<any[]>) {
    console.log(this.tracks);
    // In case the destination container is different from the previous container, we
    // need to transfer the given task to the target data array. This happens if
    // a task has been dropped on a different track.
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  onTrackDrop(event: CdkDragDrop<any[]>) {
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
  }
}
