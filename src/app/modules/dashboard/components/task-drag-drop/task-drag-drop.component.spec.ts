import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskDragDropComponent } from './task-drag-drop.component';

describe('TaskDragDropComponent', () => {
  let component: TaskDragDropComponent;
  let fixture: ComponentFixture<TaskDragDropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskDragDropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskDragDropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
