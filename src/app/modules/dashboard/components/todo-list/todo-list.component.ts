import {
  Component,
  OnInit,
  ViewChild,
  TemplateRef,
  ElementRef
} from "@angular/core";
import { TodoService } from "../../services/todo/todo.service";
import { CustomResponse } from "@app/shared/models/custom-response.model";
import { Todo } from "../../models/todo.model";
import {
  NgForm,
  NgModel,
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";

@Component({
  selector: "app-todo-list",
  templateUrl: "./todo-list.component.html",
  styleUrls: ["./todo-list.component.scss"]
})
export class TodoListComponent implements OnInit {
  isCollapsed = false;
  submitted: boolean;
  todoInput: string; // add todo title
  hoveredTodo: Todo;
  todoToEdit: Todo;

  // add todo title form control
  @ViewChild("addTodoTitle") addTodoTitleFormControl: NgModel;
  // update todo title form control
  @ViewChild("todoTitle") todoTitleFormControl: NgModel;
  // store todo title to replace it on cancel click
  selectedTodoTitle: string;

  todos: Todo[];
  todosCount: number;
  todosLoading: boolean;
  disableSaveAndCancelBtn: boolean;
  isTodoArchived: boolean;

  // modal
  modalRef: BsModalRef; // confirmation dialog modal
  moreTodoModalRef: BsModalRef; // more todo list modal

  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  // update todo title in show more modal
  updateTodoForm: FormGroup;

  constructor(
    private todoService: TodoService,
    private toastrMessageService: ToastrMessageService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.getTodos();
    this.todosLoading = true;
  }

  buildUpdateTodoForm() {
    this.updateTodoForm = new FormGroup({
      todoTitle: new FormControl("", Validators.required)
    });
  }

  /**
   * Calls get API
   */
  getTodos(): void {
    this.todoService.getTodoList().subscribe(
      (response: CustomResponse) => {
        if (response && response.status) {
          this.todos = response.data;
          this.todosCount = response.count;
        }
        this.todosLoading = false;
      },
      error => {
        this.todosLoading = false;
      }
    );
  }

  /**
   * Calls add API
   * @param form
   */
  addTodo(form: NgForm): void {
    this.closeEditMode();
    this.submitted = true;

    if (form.invalid) {
      return;
    }

    const requestBody = {
      title: form.controls["addTodoTitle"].value,
      completed: 0,
      archive: 0
    };

    this.todoService.addTodo(requestBody).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
          this.addTodoTitleFormControl.reset();
          // to remove the validation message
          this.submitted = false;
          this.getTodos();
          return;
        }
        // in case there is any field missing error
        this.toastrMessageService.showError(response.data);
        return;
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {}
    );
  }

  showAction(todo: Todo): void {
    this.hoveredTodo = todo;
  }

  hideAction(): void {
    this.hoveredTodo = null;
  }

  /**
   * Enables the edit mode of todo
   * @param todo
   */

  editTodo(todo: Todo): void {
    if (this.disableSaveAndCancelBtn) return;

    if (this.showMore) {
      this.updateTodoForm.patchValue({ todoTitle: todo.title });
    }

    this.todoToEdit = todo;
    this.selectedTodoTitle = todo.title;
  }
  disabledValue: boolean;
  /**
   * Calls update API
   * @param todo
   */
  updateTodo(todo: Todo): void {
    const todoRequestBody = this.getTodoRequestBody(todo);

    if (!todoRequestBody) {
      return;
    }

    this.disableSaveAndCancelBtn = true;
    // disable todo textarea
    this.disabledValue = true;
    this.updateTodoForm.controls["todoTitle"].disable();
    this.todoService.updateTodo(todo.id, todoRequestBody).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
        this.disableSaveAndCancelBtn = false;
      },
      () => {
        this.getTodos();
        this.disableSaveAndCancelBtn = false;
        this.todoToEdit = null;
        // enable todo textarea
        this.disabledValue = false;
        this.updateTodoForm.controls["todoTitle"].enable();
      }
    );
  }

  /**
   * Returns the todoRequestBody for updating since TDF is used in todo list & RF in show more form
   * @param todo
   */
  getTodoRequestBody(todo: Todo): boolean | any {
    let todoRequestBody;
    if (this.showMore) {
      if (this.updateTodoForm.invalid || this.disableSaveAndCancelBtn) {
        return false;
      }

      if (this.updateTodoForm.pristine) {
        this.todoToEdit = null;
        return false;
      }

      todoRequestBody = {
        id: todo.id,
        title: this.updateTodoForm.controls["todoTitle"].value
      };
    } else {
      if (this.todoTitleFormControl.invalid || this.disableSaveAndCancelBtn) {
        return false;
      }

      if (this.todoTitleFormControl.pristine) {
        this.todoToEdit = null;
        return false;
      }
      todoRequestBody = todo;
    }

    return todoRequestBody;
  }

  /**
   * Move todo item into archive list
   * @param todo
   */
  archiveTodo(todo: Todo): void {
    this.closeShowMoreModalOnLastTodoItem();
    const requestBody = {
      archive: 1
    };
    this.todoService.updateTodo(todo.id, requestBody).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
          this.isTodoArchived = true;
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getTodos();
      }
    );
  }

  /**
   * To complete todo
   * @param event
   */
  completeTodo(todo: Todo, event): void {
    let requestBody;
    if (event.target.checked) {
      requestBody = {
        completed: 1
      };
    } else {
      requestBody = {
        completed: 0
      };
    }

    this.todoService.updateTodo(todo.id, requestBody).subscribe(
      response => {
        if (response && response.status) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getTodos();
      }
    );
  }

  /**
   * Executes when user clicks on cancel button while editing
   */
  cancelUpdate(): void {
    if (this.disableSaveAndCancelBtn) return;
    if (!this.showMore)
      this.todoTitleFormControl.control.setValue(this.selectedTodoTitle);
    this.todoToEdit = null;
  }

  toggleArchivedTodo(): void {
    // to bring change on the input variable of archive component when user clicks on archive tab for calling getArchiveList API
    this.isTodoArchived = !this.isTodoArchived;
  }

  /**
   * Executes when the user clicks on archive button
   * @param todo the selected todo item
   */
  openConfirmDialog(todo: Todo): void {
    if (this.disableSaveAndCancelBtn) return;
    this.closeEditMode();
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.action = "archive";
    this.modalRef.content.data = todo.title;
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm) {
        this.archiveTodo(todo);
      }
    });
  }

  showMore: boolean;
  /**
   * Open the modal showing todo list
   * @param template
   */
  openShowMoreModal(template: TemplateRef<any>): void {
    this.showMore = true;
    this.buildUpdateTodoForm();
    this.moreTodoModalRef = this.modalService.show(template, this.config);
    this.closeEditMode();
  }

  /**
   * Close the textbox for editing
   */
  closeEditMode() {
    this.todoToEdit = null;
  }

  /**
   * Close the modal showing todo list.
   */
  closeShowMoreModal() {
    this.showMore = false;
    this.moreTodoModalRef.hide();
  }

  /**
   * Closes the showMoreModal automatically after moving the last item to the archive
   */
  closeShowMoreModalOnLastTodoItem() {
    if (this.showMore && this.todosCount <= 6) {
      // to indicate no more showMoreModal
      this.showMore = false;
      this.modalRef.hide();
      this.moreTodoModalRef.hide();
    }
  }
}
