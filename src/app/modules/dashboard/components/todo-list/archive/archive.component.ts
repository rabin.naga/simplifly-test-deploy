import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  TemplateRef
} from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { TodoService } from "@app/modules/dashboard/services/todo/todo.service";
import { Todo } from "@app/modules/dashboard/models/todo.model";
import { ToastrMessageService } from "@app/shared/services/toastr-message/toastr-message.service";

@Component({
  selector: "app-archive",
  templateUrl: "./archive.component.html",
  styleUrls: ["./archive.component.scss"]
})
export class ArchiveComponent implements OnInit, OnChanges {
  selectedArchive: any;
  archives: Todo[];
  archivesCount: number;
  archiveListLoading: boolean;

  @Input() refreshArchiveList: boolean;
  @Output() restoreArchive: EventEmitter<any> = new EventEmitter<any>();

  // modal
  moreArchivesModalRef: BsModalRef; // more archive list modal
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  constructor(
    private modalService: BsModalService,
    private todoService: TodoService,
    private toastrMessageService: ToastrMessageService
  ) {}

  ngOnInit() {
    this.archiveListLoading = true;
    this.getArchiveList();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["refreshArchiveList"]) {
      this.getArchiveList();
    }
  }

  getArchiveList(): void {
    this.todoService.getArchiveList().subscribe(
      response => {
        if (response && response.status) {
          this.archives = response.data;
          this.archivesCount = response.count;
        }
      },
      error => {
        this.archiveListLoading = false;
        this.toastrMessageService.showError(error);
      },
      () => {
        this.archiveListLoading = false;
      }
    );
  }

  showAction(archive: Todo): void {
    this.selectedArchive = archive;
  }

  hideAction(): void {
    this.selectedArchive = null;
  }

  openConfirmDialog(archiveData: Todo, action: string): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.action = action;
    this.modalRef.content.data = archiveData.title;
    this.modalRef.content.onClose.subscribe(confirm => {
      if (confirm && action === "delete") {
        this.deleteTodo(archiveData);
      }
      if (confirm && action === "restore") {
        this.restoreTodo(archiveData);
      }
    });
  }

  deleteTodo(todo: Todo): void {
    this.todoService.deleteTodo(todo.id).subscribe(
      response => {
        if (response && response.data) {
          this.toastrMessageService.showSuccess(response.data);
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getArchiveList();
      }
    );
  }

  restoreTodo(todo: Todo): void {
    const requestBody = {
      archive: 0
    };

    this.todoService.restoreToTodo(todo.id, requestBody).subscribe(
      response => {
        if (response && response.data) {
          this.toastrMessageService.showSuccess(response.data);
          this.restoreArchive.emit();
        }
      },
      error => {
        this.toastrMessageService.showError(error);
      },
      () => {
        this.getArchiveList();
      }
    );
  }

  /**
   * Open the modal showing todo list
   * @param template
   */
  openModel(template: TemplateRef<any>): void {
    this.moreArchivesModalRef = this.modalService.show(template, this.config);
    // this.closeEditMode();
  }
}
