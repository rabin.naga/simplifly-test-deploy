import { Injectable } from "@angular/core";
import { environment } from "@env/environment";
import { HttpClientService } from "@app/core/services/http-client/http-client.service";

@Injectable({
  providedIn: "root"
})
export class TrackService {
  public apiUrl = `${environment.baseIP}${environment.apiPrefix}`;
  constructor(private httpClientService: HttpClientService) {}

  getTracks() {
    return this.httpClientService.get(`${this.apiUrl}track`);
  }
}
