import { Injectable } from "@angular/core";
import { HttpClientService } from "@app/core/services/http-client/http-client.service";
import { environment } from "@env/environment";
import { Observable } from "rxjs";
import { CustomResponse } from "@app/shared/models/custom-response.model";

@Injectable({
  providedIn: "root"
})
export class TodoService {
  public apiUrl = `${environment.baseIP}${environment.apiPrefix}`;

  constructor(private readonly httpClientService: HttpClientService) {}

  // ------------------ TODO --------------------------------
  public getTodoList(): Observable<CustomResponse> {
    return this.httpClientService.get(`${this.apiUrl}todo`);
  }

  public updateTodo(id: number, body): Observable<CustomResponse> {
    return this.httpClientService.put(`${this.apiUrl}todo/${id}`, body);
  }

  public addTodo(body): Observable<any> {
    return this.httpClientService.post(`${this.apiUrl}todo`, body);
  }

  public deleteTodo(id: number): Observable<CustomResponse> {
    return this.httpClientService.delete(`${this.apiUrl}todo/${id}`);
  }

  // --------------------- Archive-----------------------------
  public getArchiveList(): Observable<CustomResponse> {
    return this.httpClientService.get(`${this.apiUrl}archive`);
  }

  public restoreToTodo(id: number, body): Observable<CustomResponse> {
    return this.httpClientService.put(`${this.apiUrl}todo/${id}`, body);
  }
}
