export class Todo {
  id: number;
  title: string;
  description: string;
  date?: string;
  completed?: number;
  archive: number;
}
