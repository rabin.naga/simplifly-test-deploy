export class Track {
    id: string;
    title: string;
    color: string;
    bgcolor: string;
    tasks: Task[]
}


export class Task {
    id: string;
    title: string;
    description: string;
    comment_count: string;
    list: string;
    track_id: string;
}