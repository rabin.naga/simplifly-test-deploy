import { Component, OnInit } from "@angular/core";
import * as $ from "jquery";
@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"]
})
export class SidebarComponent implements OnInit {
  navItems = [
    {
      id: 1,
      displayName: "Dashboard",
      iconName: "fas fa-fw fa-tachometer-alt",
      route: "/dashboard"
    },
    {
      id: 2,
      displayName: "Admin",
      iconName: "fas fa-fw fa-user",
      route: "/admin"
    },
    {
      id: 3,
      displayName: "Client",
      iconName: "fas fa-users",
      route: "/client"
    },

    {
      id: 3,
      displayName: "Class Code",
      iconName: "fas fa-fw fa-chart-area",
      route: "/class-code"
    },
    {
      id: 4,
      displayName: "Staffing Company",
      iconName: "fas fa-fw fa-table",
      route: "/staffing-company"
    },
    {
      id: 5,
      displayName: "Utilities",
      iconName: "fas fa-wrench",
      route: "/utilities"
    },
    {
      id: 7,
      displayName: "Suspect",
      iconName: "fas fa-fw fa-user",
      route: "/suspect"
    },
    {
      id: 7,
      displayName: "Prospect",
      iconName: "fas fa-users",
      route: "/prospect"
    },
    {
      id: 8,
      displayName: "Message",
      iconName: "fas fa-envelope",
      route: "/message"
    }
    // {
    //   id: 6,
    //   displayName: "Nested Dropdown",
    //   iconName: "fas fa-fw fa-user",
    //   children: [
    //     {
    //       displayName: "Speakers2",
    //       iconName: "group",
    //       children: [
    //         {
    //           displayName: "Michael Prentice",
    //           iconName: "person",
    //           route: "michael-prentice",
    //           children: [
    //             {
    //               displayName: "Create Enterprise UIs",
    //               iconName: "star_rate",
    //               route: "material-design"
    //             }
    //           ]
    //         },
    //         {
    //           displayName: "Stephen Fluin",
    //           iconName: "person",
    //           route: "stephen-fluin",
    //           children: [
    //             {
    //               displayName: "What's up with the Web?",
    //               iconName: "star_rate",
    //               route: "what-up-web"
    //             }
    //           ]
    //         },
    //         {
    //           displayName: "Mike Brocchi",
    //           iconName: "person",
    //           route: "mike-brocchi",
    //           children: [
    //             {
    //               displayName: "My ally, the CLI",
    //               iconName: "star_rate",
    //               route: "my-ally-cli"
    //             },
    //             {
    //               displayName: "Become an Angular Tailor",
    //               iconName: "star_rate",
    //               route: "become-angular-tailer"
    //             }
    //           ]
    //         }
    //       ]
    //     },
    //     {
    //       displayName: "Sessions3",
    //       iconName: "speaker_notes",
    //       children: [
    //         {
    //           displayName: "Create Enterprise UIs",
    //           iconName: "star_rate",
    //           route: "material-design"
    //         },
    //         {
    //           displayName: "What's up with the Web?",
    //           iconName: "star_rate",
    //           route: "what-up-web"
    //         },
    //         {
    //           displayName: "My ally, the CLI",
    //           iconName: "star_rate",
    //           route: "my-ally-cli"
    //         },
    //         {
    //           displayName: "Become an Angular Tailor",
    //           iconName: "star_rate",
    //           route: "become-angular-tailer"
    //         }
    //       ]
    //     },
    //     {
    //       displayName: "Feedback3",
    //       iconName: "feedback",
    //       route: "feedback"
    //     }
    //   ]
    // }
  ];
  constructor() {}

  ngOnInit() {}

  toogleSideNav() {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
  }
}
