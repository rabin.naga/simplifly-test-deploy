import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminPanelComponent } from "./core/layout/admin-panel/admin-panel.component";

const routes: Routes = [
  {
    path: "login",
    loadChildren: "@modules/auth/login/login.module#LoginModule"
  },
  {
    path: "register",
    loadChildren: "@modules/auth/register/register.module#RegisterModule"
  },
  {
    path: "",
    component: AdminPanelComponent,
    data: {
      breadcrumb: "Dashboard"
    },
    children: [
      { path: "", redirectTo: "dashboard", pathMatch: "full" },
      {
        path: "dashboard",
        loadChildren: "@modules/dashboard/dashboard.module#DashboardModule"
      },
      {
        path: "admin",
        loadChildren: "@modules/admin/admin.module#AdminModule",
        data: {
          breadcrumb: "Admin"
        }
      },
      {
        path: "client",
        loadChildren: "@modules/client/client.module#ClientModule",
        data: {
          breadcrumb: "Client"
        }
      },
      {
        path: "class-code",
        loadChildren: "@modules/class-code/class-code.module#ClassCodeModule",
        data: {
          breadcrumb: "Class Code"
        }
      },
      {
        path: "message",
        loadChildren: "@modules/message/message.module#MessageModule",
        data: {
          breadcrumb: "Message"
        }
      },
      {
        path: "staffing-company",
        loadChildren:
          "@modules/staffing-company/staffing-company.module#StaffingCompanyModule",
        data: {
          breadcrumb: "Staffing Company"
        }
      },
      {
        path: "utilities",
        loadChildren: "@modules/utility/utility.module#UtilityModule",
        data: {
          breadcrumb: "Utilities"
        }
      },
      {
        path: "suspect",
        loadChildren: "@modules/suspect/suspect.module#SuspectModule",
        data: {
          breadcrumb: "Suspect"
        }
      },
      {
        path: "prospect",
        loadChildren: "@modules/prospect/prospect.module#ProspectModule",
        data: {
          breadcrumb: "Prospect"
        }
      },
      { path: "**", loadChildren: "@modules/error/error.module#ErrorModule" }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
