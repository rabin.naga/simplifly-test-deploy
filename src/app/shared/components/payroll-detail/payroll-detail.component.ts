import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";

@Component({
  selector: "simpliflysaas-payroll-detail",
  templateUrl: "./payroll-detail.component.html",
  styleUrls: ["./payroll-detail.component.scss"]
})
export class PayrollDetailComponent implements OnInit {
  payrollDetailform: FormGroup;
  submitted: boolean;
  language: string;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.buildPayrollDetailform();
  }
  buildPayrollDetailform() {
    this.payrollDetailform = this.fb.group({
      payrollDetail: this.fb.array([this.addPayrollDetailFormGroup()])
    });
  }
  addPayrollDetailFormGroup(): FormGroup {
    return this.fb.group({
      employeeName: ["", Validators.required],
      hours: [""],
      typeOfPay: ["", Validators.required],
      billRate: [""],
      workersCCode: ["", Validators.required],
      classification: [""],
      location: [""],
      department: [""],
      date: [""],
      amount: [""],
      notInv: [""],
      notPay: [""]
    });
  }
  addBox() {
    this.submitted = false;

    (<FormArray>this.payrollDetailform.get("payrollDetail")).push(
      this.addPayrollDetailFormGroup()
    );
  }
  deletBox(i: number) {
    (<FormArray>this.payrollDetailform.get("payrollDetail")).removeAt(i);
  }
  OnCreateClick() {
    this.submitted = true;
    // after create put api is called
    console.log("form is created sucessfully");
    console.log(this.payrollDetailform.value);
  }
  get payrolldetailFormData() {
    return <FormArray>this.payrollDetailform.get("payrollDetail");
  }
}
