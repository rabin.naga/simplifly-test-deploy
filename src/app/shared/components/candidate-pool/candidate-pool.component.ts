import { CandidatePoolService } from "./../../services/candidate-pool/candidate-pool.service";
import { CandidatePool } from "./model/canditate-pool-model";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "simpliflysaas-candidate-pool",
  templateUrl: "./candidate-pool.component.html",
  styleUrls: ["./candidate-pool.component.scss"]
})
export class CandidatePoolComponent implements OnInit {
  candidatePoolList: CandidatePool[];
  candidatePoolLoading: boolean;
  noResult: boolean;
  fieldSearchTerm: any;
  selectedSearchTerm: any;

  constructor(private candidatePoolService: CandidatePoolService) {}

  ngOnInit() {
    this.getCandidatePoolService();
  }

  getCandidatePoolService() {
    this.candidatePoolLoading = true;
    this.candidatePoolService.getCandidatePoolList().subscribe(
      response => {
        this.candidatePoolList = response;
        if (JSON.stringify(response.length) == "0") {
          this.noResult = true;
        }
      },
      error => {
        this.candidatePoolLoading = false;
      },
      () => {
        this.candidatePoolLoading = false;
      }
    );
  }
}
