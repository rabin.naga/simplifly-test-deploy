import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { QuestionBase } from "../../../../../projects/simpliflysaas-data-table/src/lib/models/question-base";

@Component({
  selector: "simpliflysaas-dynamic-form",
  templateUrl: "./dynamic-form.component.html",
  styleUrls: ["./dynamic-form.component.scss"]
})
export class DynamicFormComponent implements OnInit, OnChanges {
  @Input() form: FormGroup;
  @Input() formCtrl: QuestionBase<any>;
  @Input() submitted: boolean;

  get isValid() {
    return this.form.controls[this.formCtrl.key].valid;
  }
  constructor() {}

  ngOnInit() {}

  ngOnChanges() {}
}
