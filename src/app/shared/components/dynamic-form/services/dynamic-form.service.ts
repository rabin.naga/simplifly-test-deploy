import { Injectable } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";

@Injectable({
  providedIn: "root"
})
export class DynamicFormService {
  constructor() {}

  createFormGroup(questions) {
    let group: any = {};
    let validatorsArray = [];
    questions.forEach(question => {
      // validation
      if (question.required) {
        validatorsArray.push(Validators.required);
      }

      if (question.minLength) {
        validatorsArray.push(Validators.minLength(question.minLength));
      }

      if (question.maxLength) {
        validatorsArray.push(Validators.maxLength(question.maxLength));
      }

      if (question.pattern) {
        validatorsArray.push(Validators.pattern(question.pattern));
      }

      // check for minlength or maxlength or pattern also
      group[question.key] = question.required
        ? new FormControl(question.value || "", validatorsArray)
        : new FormControl(question.value || "");

      validatorsArray = [];
    });
    return new FormGroup(group);
  }
}
