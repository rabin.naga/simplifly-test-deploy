import { Injectable } from "@angular/core";
import { environment } from "@env/environment";
import { Observable } from "rxjs";
import { ApiRouteConst } from "@app/shared/constants/api-route.constant";
import { HttpClientService } from "@app/core/services/http-client/http-client.service";

import { CandidatePool } from "@shared/components/candidate-pool/model/canditate-pool-model";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class CandidatePoolService {
  private _url = "http://localhost:3000/candidatePool";
  baseIp = environment.baseIP;
  apiPrefix = environment.apiPrefix;

  constructor(
    private httpClientService: HttpClientService,
    // this is for json server
    private http: HttpClient
  ) {}

  getCandidatePoolList(): Observable<CandidatePool[]> {
    return this.http.get<CandidatePool[]>(this._url);
  }

  //   getClientByClientId(clientId): Observable<Iclient> {
  //     return this.http.get<Iclient>(`${this._url}/${clientId}`);
  //   }

  //   addClient(body): Observable<Iclient> {
  //     return this.http.post<Iclient>(this._url, body, {
  //       headers: new HttpHeaders({
  //         "Content-type": "application/json"
  //       })
  //     });
  //   }
}

// this service is made for dummy api...using json server..
