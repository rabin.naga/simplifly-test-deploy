import { Injectable } from "@angular/core";
@Injectable({
  providedIn: "root"
})
export class HelperService {
  constructor() {}

  /**
   * This method is to return the full name of the user
   */
  getFullName(person: any) {
    if (person.middle_name) {
      const fullName =
        person.first_name + " " + person.middle_name + " " + person.last_name;
      return fullName;
    } else {
      const fullName = person.first_name + " " + person.last_name;
      return fullName;
    }
  }

  /**
   * replace hyphen by space
   * @param value
   */
  replaceHyphenWithSpace(value) {
    let i = 0;
    while (value.includes("-")) {
      value = value.replace("-", " ");
    }
    // returns the first character of the string capital
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
}
