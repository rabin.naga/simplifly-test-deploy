import { CycleComponent } from "@app/modules/dashboard/components/cycle/cycle.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CollapseModule } from "ngx-bootstrap/collapse";
import {
  TooltipModule,
  TabsModule,
  ModalModule,
  TimepickerModule,
  BsDatepickerModule,
  BsDropdownModule
} from "ngx-bootstrap";
import { PaginationComponent } from "./components/pagination/pagination.component";
import { NgxPaginationModule } from "ngx-pagination";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { OrderModule } from "ngx-order-pipe";
import { DynamicFormComponent } from "./components/dynamic-form/dynamic-form.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { MatStepperModule } from "@angular/material/stepper";
import { LanguageSwitcherComponent } from "./components/language-switcher/language-switcher.component";
import { TextMaskModule } from "angular2-text-mask";
import { CandidatePoolComponent } from "./components/candidate-pool/candidate-pool.component";
import { PayrollDetailComponent } from "./components/payroll-detail/payroll-detail.component";

@NgModule({
  declarations: [
    PaginationComponent,
    DynamicFormComponent,
    LanguageSwitcherComponent,
    CandidatePoolComponent,
    CycleComponent,
    PayrollDetailComponent
  ],
  imports: [
    CommonModule,
    CollapseModule.forRoot(),
    TooltipModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    NgxPaginationModule,
    Ng2SearchPipeModule,
    OrderModule,
    FormsModule,
    ReactiveFormsModule,
    MatStepperModule,
    TextMaskModule,
    TimepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot()
  ],
  exports: [
    CollapseModule,
    TooltipModule,
    TabsModule,
    ModalModule,
    PaginationComponent,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    OrderModule,
    FormsModule,
    ReactiveFormsModule,
    MatStepperModule,
    DynamicFormComponent,
    LanguageSwitcherComponent,
    TextMaskModule,
    TimepickerModule,
    BsDatepickerModule,
    CandidatePoolComponent,
    CycleComponent,
    PayrollDetailComponent,
    BsDropdownModule
  ]
})
export class SharedModule {}
