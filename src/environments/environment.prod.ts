export const environment = {
  production: true,
  baseIP: "http://simpliflybackend.bentray.work/",
  // baseIP: "http://localhost/Dev/simpliflysaas/",
  apiPrefix: "api/web/v1/",
  defaultImageUrl: "./assets/images/user.jpg"
};
