export class QuestionBase<T> {
  value: T;
  key: string;
  label: string;
  required: boolean;
  minLength: number;
  maxLength: number;
  pattern: string;
  order: number;
  controlType: string;
  sort: boolean;
  search: boolean;

  constructor(
    options: {
      value?: T;
      key?: string;
      label?: string;
      required?: boolean;
      minLength?: number;
      maxLength?: number;
      pattern?: string;
      order?: number;
      controlType?: string;
      sort?: boolean;
      search?: boolean;
    } = {}
  ) {
    this.value = options.value;
    this.key = options.key || "";
    this.label = options.label || "";
    this.required = !!options.required;
    this.minLength = options.minLength;
    this.maxLength = options.maxLength;
    this.pattern = options.pattern;
    this.order = options.order === undefined ? 1 : options.order;
    this.controlType = options.controlType || "";
    this.sort = options.sort || false;
    this.search = options.search || false;
  }
}
