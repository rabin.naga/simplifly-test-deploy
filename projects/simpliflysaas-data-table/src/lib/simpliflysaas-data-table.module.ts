import { NgModule } from "@angular/core";
import { SimpliflysaasDataTableComponent } from "./simpliflysaas-data-table.component";
import { DataTablePaginationService } from "./services/data-table-pagination.service";
import { QuestionControlService } from "./services/question-control.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DynamicFormQuestionComponent } from "./components/dynamic-form-question/dynamic-form-question.component";
import { CommonModule } from "@angular/common";
import { TooltipModule } from "ngx-bootstrap";

@NgModule({
  declarations: [SimpliflysaasDataTableComponent, DynamicFormQuestionComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TooltipModule.forRoot()
  ],
  exports: [SimpliflysaasDataTableComponent, DynamicFormQuestionComponent],
  providers: [DataTablePaginationService, QuestionControlService]
})
export class SimpliflysaasDataTableModule {}
