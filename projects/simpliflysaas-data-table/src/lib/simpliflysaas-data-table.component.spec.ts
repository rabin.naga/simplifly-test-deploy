import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SimpliflysaasDataTableComponent } from "./simpliflysaas-data-table.component";

describe("SimpliflysaasDataTableComponent", () => {
  let component: SimpliflysaasDataTableComponent;
  let fixture: ComponentFixture<SimpliflysaasDataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SimpliflysaasDataTableComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpliflysaasDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
